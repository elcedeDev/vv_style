import QtQuick 1.1
import "Style.js" as S

Item {
    id: root
    property variant currentStyle: S[currentStyleName]

    property string currentStyleName: S.styleName //"Aradex"

    signal styleChanged(string newStyle)
    onStyleChanged: {
        currentStyleName = newStyle;
        currentStyle = S.Style[currentStyleName]
    }

    signal newStyleAdded(string strNewStyleName, variant newStyleObject)
    onNewStyleAdded: {
        S.Style[strNewStyleName] = newStyleObject.Style[strNewStyleName]
    }

}
