// $Revision: 21117 $
.pragma library

var Desktop = new Object();

//////////////////////////////////
//            GENERAL           //
//////////////////////////////////
Desktop.imageFolder = Qt.resolvedUrl("images/");

Desktop.fontFamily = "Arial"
Desktop.fontFamilyBold = "Arial Black"
Desktop.fontSize = 10
Desktop.backgroundColor = "#F0F0F0"
Desktop.lightColor = "#F0F0F0"
Desktop.mediumColor = "#EBEBEB"
Desktop.lightFontColor = "#787878"
Desktop.subLightFontColor = "#AAAAAA"
Desktop.mediumFontColor =  "#000000"
Desktop.subMediumFontColor = "#3E3E3E"
Desktop.errorImage = "icons/Error.svg"
Desktop.warningImage = "icons/Warning.svg"
Desktop.messageImage = "icons/Message.svg"

//////////////////////////////////
//            FOCUS             //
//////////////////////////////////
Desktop.focusBorderLeft = 7
Desktop.focusBorderRight = 7
Desktop.focusBorderTop = 7
Desktop.focusBorderBottom = 7
Desktop.focusShiftLeft = -1
Desktop.focusShiftRight = -1
Desktop.focusShiftTop = -1
Desktop.focusShiftBottom = -1
Desktop.focusOpacity = 1
Desktop.focusImage = ""

//////////////////////////////////
//            THEME             //
//////////////////////////////////
Desktop.themeImage0 = "winxp_segment.png"
Desktop.themeImage1 = "winxp_segment.png"
Desktop.themeImage2 = "winxp_segment.png"
Desktop.themeImage3 = "winxp_segment.png"
Desktop.themeImage4 = "winxp_segment.png"
Desktop.themeImage5 = ""
Desktop.themeImage6 = ""
Desktop.themeImage7 = ""
Desktop.themeColor0 = "#848484"
Desktop.themeColor1 = "#F0F0F0"
Desktop.themeColor2 = "#EBEBEB"
Desktop.themeColor3 = "#787878"
Desktop.themeColor4 = "#AAAAAA"
Desktop.themeColor5 = ""
Desktop.themeColor6 = ""
Desktop.themeColor7 = ""
Desktop.themeHasImage = true
Desktop.themeBorder = 3
Desktop.themeBorderLeft = Desktop.themeBorder
Desktop.themeBorderRight = Desktop.themeBorder
Desktop.themeBorderTop = Desktop.themeBorder
Desktop.themeBorderBottom = Desktop.themeBorder

//////////////////////////////////
//       VIRTUAL KEYBOARD       //
//////////////////////////////////
Desktop.virtualKeyboardBackgroundColor = Desktop.themeColor3
Desktop.virtualKeyboardKeyBackgroundColor = Desktop.themeColor2
Desktop.virtualKeyboardKeyTextColor = "black"
Desktop.virtualKeyboardControlKeyBackgroundColor = Desktop.themeColor4
Desktop.virtualKeyboardControlKeyTextColor = "white"
Desktop.virtualKeyboardControlKeyTextActiveColor = "black"

//////////////////////////////////
//          CONTAINER           //
//////////////////////////////////
// Colors correspond to Theme Colors above
// [main themeshape colorchoice, header themeshape colorchoice, fontcolor]
Desktop.containerColors0 = [0,1,1]
Desktop.containerColors1 = [1,1,1]
Desktop.containerColors2 = [0,1,1]
Desktop.containerColors3 = [1,0,1]
Desktop.containerHeaderHeight = 25
Desktop.containerHeaderWidth = 100
Desktop.containerHeaderVisible = true
Desktop.containerHeaderTextInBorder = false
Desktop.containerFitText = true
Desktop.containerTextXShift = Desktop.fontSize
Desktop.containerTextYShift = Desktop.fontSize


//////////////////////////////////
//           Segment            //
//////////////////////////////////
Desktop.segmentImage0 = "winxp_segment.png"
Desktop.segmentImage1 = "winxp_segment.png"
Desktop.segmentImage2 = "winxp_segment.png"
Desktop.segmentImage3 = "winxp_segment.png"
Desktop.segmentImage4 = "winxp_segment.png"
Desktop.segmentImage5 = "winxp_segment.png"
Desktop.segmentImage6 = ""
Desktop.segmentImage7 = ""
Desktop.segmentImageHeight = 40
Desktop.segmentBorderLeft = 6
Desktop.segmentBorderRight = 6
Desktop.segmentBorderTop = 6
Desktop.segmentBorderBottom = 6

Desktop.segmentHeight = 200
Desktop.segmentWidth = 120

//////////////////////////////////
//          LABELLIST           //
//////////////////////////////////
Desktop.labellistColor0 = Desktop.themeColor0
Desktop.labellistColor1 = Desktop.themeColor1
Desktop.labellistColor2 = Desktop.themeColor2
Desktop.labellistColor3 = Desktop.themeColor3
Desktop.labellistColor4 = Desktop.themeColor4
Desktop.labellistLeftRightMargin = 4
Desktop.labellistSeperatorWidth = 1



//////////////////////////////////
//            BUTTON            //
//////////////////////////////////
Desktop.buttonImageNormal = "winxp_button_normal.png"
Desktop.buttonImagePressed = "winxp_button_pressed.png"
Desktop.buttonImageDisabled = "winxp_button_disabled.png"
Desktop.buttonImageFocus = Desktop.focusImage
Desktop.buttonBorderSize = 7;
Desktop.buttonTextBorderSize = 3
Desktop.buttonFontSize = Desktop.fontSize
Desktop.buttonTextMargin = 3;
Desktop.buttonFontColor = Desktop.mediumFontColor
Desktop.buttonFontPressedColor = Desktop.mediumFontColor
Desktop.buttonFontDisabledColor = Desktop.subLightFontColor
Desktop.buttonFontFamily = Desktop.fontFamily
// Image Borders
Desktop.buttonBorderLeftSize = Desktop.buttonBorderSize;
Desktop.buttonBorderRightSize = Desktop.buttonBorderSize;
Desktop.buttonBorderTopSize = Desktop.buttonBorderSize;
Desktop.buttonBorderBottomSize = Desktop.buttonBorderSize;

// Text Borders
Desktop.buttonTextBorderLeftSize = Desktop.buttonTextBorderSize;
Desktop.buttonTextBorderRightSize = Desktop.buttonTextBorderSize;
Desktop.buttonTextBorderTopSize = Desktop.buttonTextBorderSize;
Desktop.buttonTextBorderBottomSize = Desktop.buttonTextBorderSize;

//margins
Desktop.buttonTextLeftMargin = 5;
Desktop.buttonTextRightMargin = 0;
Desktop.buttonTextTopMargin = 5;
Desktop.buttonTextBottomMargin = 0;
// text Alignement
Desktop.buttonHorizontalAlignement = Qt.AlignHCenter
Desktop.buttonVerticalAlignement = Qt.AlignVCenter
// Button Shift when Pressed
Desktop.buttonPressedLeftShift = 0
Desktop.buttonPressedRightShift = 0
Desktop.buttonPressedTopShift = 0
Desktop.buttonPressedBottomShift = 0
// Animations
Desktop.buttonSizeAnimationTime = 0
Desktop.buttonTextAnimationTime = 0
// Focus
Desktop.buttonFocusBorderLeft = Desktop.focusBorderLeft
Desktop.buttonFocusBorderRight = Desktop.focusBorderRight
Desktop.buttonFocusBorderTop = Desktop.focusBorderTop
Desktop.buttonFocusBorderBottom = Desktop.focusBorderBottom
Desktop.buttonFocusShiftLeft = Desktop.focusShiftLeft
Desktop.buttonFocusShiftRight = Desktop.focusShiftRight
Desktop.buttonFocusShiftTop = Desktop.focusShiftTop
Desktop.buttonFocusShiftBottom = Desktop.focusShiftBottom
// forground button
Desktop.buttonForegroundImageHeight = 32
Desktop.buttonForegroundImageWidth = 32

//////////////////////////////////
//       LANGUAGE BUTTON        //
//////////////////////////////////

Desktop.languageButtonImage = new Object()
Desktop.languageButtonImage["en"] = "lang/United-Kingdom-64.png"
Desktop.languageButtonImage["de"] = "lang/Germany-64.png"
Desktop.languageButtonImage["fr"] = "lang/France-64.png"
Desktop.languageButtonImage["sp"] = "lang/Spain-64.png"
Desktop.languageButtonImage["zh"] = "lang/China-64.png"
Desktop.languagebuttonImageHeight = 32
Desktop.languagebuttonImageWidth = 32
Desktop.languagebuttonImageHorizontalAlignement = Qt.AlignLeft
Desktop.languagebuttonImageVerticalAlignement = Qt.AlignVCenter
Desktop.languagebuttonImageXShift = 8
Desktop.languagebuttonImageYShift = 0

//////////////////////////////////
//         logFile BUTTON        //
//////////////////////////////////
Aradex.logFileButtonImage= "icons/logFile.svg"
Aradex.logFileButtonImageHeight = 48
Aradex.logFileButtonImageWidth = 48
Aradex.logFileButtonImageHorizontalAlignement = Qt.AlignHCenter
Aradex.logFileButtonImageVerticalAlignement = Qt.AlignVCenter
Aradex.logFileButtonImageXShift = 0
Aradex.logFileButtonImageYShift = 0

//////////////////////////////////
//         STYLE  BUTTON        //
//////////////////////////////////
Desktop.styleButtonImage = new Object()
Desktop.styleButtonImage["Aradex"] = ""
Desktop.styleButtonImage["Desktop"] = ""
Desktop.stylebuttonImageHeight = 36
Desktop.stylebuttonImageWidth = 36
Desktop.stylebuttonImageHorizontalAlignement = Qt.AlignRight
Desktop.stylebuttonImageVerticalAlignement = Qt.AlignVCenter
Desktop.stylebuttonImageXShift = - 8
Desktop.stylebuttonImageYShift = 0

//////////////////////////////////
//            TOGGLEBUTTON      //
//////////////////////////////////
Desktop.togglebuttonImageNormal = ""
Desktop.togglebuttonImagePressed = ""
Desktop.togglebuttonImageDisabled = ""
Desktop.togglebuttonImagePressedAndDisabled = ""
Desktop.togglebuttonImageFocus = Desktop.focusImage
Desktop.togglebuttonLightImageOff = "winxp_checkbox_normal.png"
Desktop.togglebuttonLightImageOn = "winxp_checkbox_checked.png"
Desktop.togglebuttonLightHorizontalAlignement = Qt.AlignRight
Desktop.togglebuttonLightVerticalAlignement = Qt.AlignVCenter
Desktop.togglebuttonLightImageHeight = 13
Desktop.togglebuttonLightImageWidth = 13

Desktop.togglebuttonBorderSize = 7;
Desktop.togglebuttonTextBorderSize = 3
Desktop.togglebuttonFontSize = Desktop.fontSize
Desktop.togglebuttonTextMargin = 3;
Desktop.togglebuttonFontColor = Desktop.mediumFontColor
Desktop.togglebuttonFontPressedColor = Desktop.mediumFontColor
Desktop.togglebuttonFontDisabledColor = Desktop.lightFontColor
Desktop.togglebuttonFontPressedAndDisabledColor = Desktop.lightFontColor
Desktop.togglebuttonFocusBorderColor = "#00000000"
Desktop.togglebuttonFontFamily = Desktop.fontFamily
// Image Borders
Desktop.togglebuttonBorderLeftSize = Desktop.togglebuttonBorderSize;
Desktop.togglebuttonBorderRightSize = Desktop.togglebuttonBorderSize;
Desktop.togglebuttonBorderTopSize = Desktop.togglebuttonBorderSize;
Desktop.togglebuttonBorderBottomSize = Desktop.togglebuttonBorderSize;

// Text Borders
Desktop.togglebuttonTextBorderLeftSize = Desktop.togglebuttonTextBorderSize;
Desktop.togglebuttonTextBorderRightSize = Desktop.togglebuttonTextBorderSize;
Desktop.togglebuttonTextBorderTopSize = Desktop.togglebuttonTextBorderSize;
Desktop.togglebuttonTextBorderBottomSize = Desktop.togglebuttonTextBorderSize;

//margins
Desktop.togglebuttonTextLeftMargin = Desktop.togglebuttonLightImageWidth * 1.5;
Desktop.togglebuttonTextRightMargin = 0;
Desktop.togglebuttonTextTopMargin = 0;
Desktop.togglebuttonTextBottomMargin = 0;
// text Alignement
Desktop.togglebuttonHorizontalAlignement = Qt.AlignLeft
Desktop.togglebuttonVerticalAlignement = Qt.AlignVCenter

// Button Shift when Pressed
Desktop.togglebuttonPressedLeftShift = 0
Desktop.togglebuttonPressedRightShift = 0
Desktop.togglebuttonPressedTopShift = 0
Desktop.togglebuttonPressedBottomShift = 0
// Animations
Desktop.togglebuttonSizeAnimationTime = 0
Desktop.togglebuttonTextAnimationTime = 0
// Focus
Desktop.togglebuttonFocusBorderLeft = Desktop.focusBorderLeft
Desktop.togglebuttonFocusBorderRight = Desktop.focusBorderRight
Desktop.togglebuttonFocusBorderTop = Desktop.focusBorderTop
Desktop.togglebuttonFocusBorderBottom = Desktop.focusBorderBottom
Desktop.togglebuttonFocusShiftLeft = Desktop.focusShiftLeft
Desktop.togglebuttonFocusShiftRight = Desktop.focusShiftRight
Desktop.togglebuttonFocusShiftTop = Desktop.focusShiftTop
Desktop.togglebuttonFocusShiftBottom = Desktop.focusShiftBottom
// forground button
Desktop.togglebuttonForegroundImageHeight = 32
Desktop.togglebuttonForegroundImageWidth = 32

//////////////////////////////////
//          RADIOBUTTON         //
//////////////////////////////////
Desktop.radiobuttonImageNormal = "winxp_button_normal.png"
Desktop.radiobuttonImagePressed = "winxp_button_pressed.png"
Desktop.radiobuttonImageDisabled = "winxp_button_disabled.png"
Desktop.radiobuttonImagePressedAndDisabled = "winxp_button_disabled.png"
Desktop.radiobuttonImageFocus = Desktop.focusImage
Desktop.radiobuttonLightImageOff = ""
Desktop.radiobuttonLightImageOn = ""
Desktop.radiobuttonLightImageHeight = 30
Desktop.radiobuttonLightImageWidth = 30
Desktop.radiobuttonLightHorizontalAlignement = Qt.AlignRight
Desktop.radiobuttonLightVerticalAlignement = Qt.AlignTop
Desktop.radiobuttonBorderSize = 7;
Desktop.radiobuttonTextBorderSize = 3
Desktop.radiobuttonFontSize = Desktop.fontSize
Desktop.radiobuttonTextMargin = 3;
Desktop.radiobuttonFontColor = Desktop.mediumFontColor
Desktop.radiobuttonFontPressedColor = Desktop.mediumFontColor
Desktop.radiobuttonFontDisabledColor = Desktop.subLightFontColor
Desktop.radiobuttonFontPressedAndDisabledColor = Desktop.lightFontColor
Desktop.radiobuttonFocusBorderColor = "#00000000"
Desktop.radiobuttonFontFamily = Desktop.fontFamily
// Image Borders
Desktop.radiobuttonBorderLeftSize = Desktop.radiobuttonBorderSize;
Desktop.radiobuttonBorderRightSize = Desktop.radiobuttonBorderSize;
Desktop.radiobuttonBorderTopSize = Desktop.radiobuttonBorderSize;
Desktop.radiobuttonBorderBottomSize = Desktop.radiobuttonBorderSize;
// Text Borders
Desktop.radiobuttonTextBorderLeftSize = Desktop.radiobuttonTextBorderSize;
Desktop.radiobuttonTextBorderRightSize = Desktop.radiobuttonTextBorderSize;
Desktop.radiobuttonTextBorderTopSize = Desktop.radiobuttonTextBorderSize;
Desktop.radiobuttonTextBorderBottomSize = Desktop.radiobuttonTextBorderSize;

//margins
Desktop.radiobuttonTextLeftMargin = 5;
Desktop.radiobuttonTextRightMargin = 0;
Desktop.radiobuttonTextTopMargin = 5;
Desktop.radiobuttonTextBottomMargin = 0;
// text Alignement
Desktop.radiobuttonHorizontalAlignement = Qt.AlignHCenter
Desktop.radiobuttonVerticalAlignement = Qt.AlignVCenter

// Button Shift when Pressed
Desktop.radiobuttonPressedLeftShift = 0
Desktop.radiobuttonPressedRightShift = 0
Desktop.radiobuttonPressedTopShift = 0
Desktop.radiobuttonPressedBottomShift = 0
// Animations
Desktop.radiobuttonSizeAnimationTime = 0
Desktop.radiobuttonTextAnimationTime = 0
// Focus
Desktop.radiobuttonFocusBorderLeft = Desktop.focusBorderLeft
Desktop.radiobuttonFocusBorderRight = Desktop.focusBorderRight
Desktop.radiobuttonFocusBorderTop = Desktop.focusBorderTop
Desktop.radiobuttonFocusBorderBottom = Desktop.focusBorderBottom
Desktop.radiobuttonFocusShiftLeft = Desktop.focusShiftLeft
Desktop.radiobuttonFocusShiftRight = Desktop.focusShiftRight
Desktop.radiobuttonFocusShiftTop = Desktop.focusShiftTop
Desktop.radiobuttonFocusShiftBottom = Desktop.focusShiftBottom

//////////////////////////////////
//             Label            //
//////////////////////////////////
Desktop.labelImage0 = ""
Desktop.labelImage1 = ""
Desktop.labelImage2 = ""
Desktop.labelImage3 = ""
Desktop.labelFontColor0 = Desktop.mediumFontColor
Desktop.labelFontColor1 = Desktop.subMediumFontColor
Desktop.labelFontColor2 = Desktop.lightFontColor
Desktop.labelFontColor3 = Desktop.subLightFontColor
Desktop.labelBorderSize = 4;
Desktop.labelTextBorderSize = 2
Desktop.labelFontSize = Desktop.fontSize
Desktop.labelTextMargin = 3;
Desktop.labelFontColor = Desktop.mediumFontColor
Desktop.labelFocusBorderColor = "#00000000"
// text Alignement
Desktop.labelHorizontalAlignement = Qt.AlignLeft
Desktop.labelVerticalAlignement = Qt.AlignVCenter
Desktop.labelFontFamily = Desktop.fontFamily
// image borders
Desktop.labelTextBorderLeftSize = Desktop.labelTextBorderSize
Desktop.labelTextBorderRightSize = Desktop.labelTextBorderSize
Desktop.labelTextBorderTopSize = Desktop.labelTextBorderSize
Desktop.labelTextBorderBottomSize = Desktop.labelTextBorderSize
// text Borders
Desktop.labelBorderLeftSize = Desktop.labelBorderSize
Desktop.labelBorderRightSize = Desktop.labelBorderSize
Desktop.labelBorderTopSize = Desktop.labelBorderSize
Desktop.labelBorderBottomSize = Desktop.labelBorderSize
// margins
Desktop.labelTextLeftMargin = Desktop.labelTextMargin
Desktop.labelTextRightMargin = 0//Desktop.labelTextMargin
Desktop.labelTextTopMargin = 0//Desktop.labelTextMargin
Desktop.labelTextBottomMargin = Desktop.labelTextMargin
// animation
Desktop.labelAnimationHighlightColor = "#0000FF"
Desktop.labelAnimationHighlightHoldTime = 1000
Desktop.labelAnimationHighlightTransitionTime = 2500


//////////////////////////////////
//          TextField           //
//////////////////////////////////
Desktop.textfieldImageEnabled = "winxp_textfield_normal.png"
Desktop.textfieldImageFocus = "winxp_textfield_selected.png"
Desktop.textfieldBorderSize = 2
Desktop.textfieldFocusBorderSize = 2
Desktop.textfieldTextBorderSize = 3
Desktop.textfieldFontSize = Desktop.fontSize;
Desktop.textfieldFontColor = Desktop.mediumFontColor

Desktop.fontColor0 = Desktop.mediumFontColor
Desktop.fontColor1 = Desktop.subMediumFontColor
Desktop.fontColor2 = Desktop.lightFontColor
Desktop.fontColor3 = Desktop.subLightFontColor

Desktop.textfieldSelectionColor = Desktop.subLightFontColor
Desktop.textfieldSelectedTextColor = Desktop.mediumFontColor
Desktop.textfieldFontFamily = Desktop.fontFamily
Desktop.textfieldHorizontalAlignement = Qt.AlignLeft
// Disabled - read-only
Desktop.textfieldImageDisabled = "winxp_textfield_disabled.png"
Desktop.textfieldImageReadOnly = "winxp_textfield_disabled.png"
Desktop.textfieldFontReadOnlyColor = Desktop.subMediumFontColor
Desktop.textfieldFontInactiveColor = Desktop.subLightFontColor
Desktop.textfieldDisabledOpacity = 0.7
Desktop.textfieldReadOnlyOpacity = 0.7
// Placeholder Text
Desktop.textfieldPlaceholderTextSize = Desktop.textfieldFontSize
Desktop.textfieldFontPlaceholderColor = Desktop.lightFontColor
Desktop.textfieldPlaceholderOpacity = 0.3

// image Borders
Desktop.textfieldBorderLeftSize = Desktop.textfieldBorderSize;
Desktop.textfieldBorderRightSize = Desktop.textfieldBorderSize;
Desktop.textfieldBorderTopSize = Desktop.textfieldBorderSize;
Desktop.textfieldBorderBottomSize = Desktop.textfieldBorderSize;

// image text Borders
Desktop.textfieldTextBorderLeftSize = 4
Desktop.textfieldTextBorderRightSize = 4
Desktop.textfieldTextBorderTopSize = 4
Desktop.textfieldTextBorderBottomSize = 4

// Focus
Desktop.textfieldFocusBorderLeft = Desktop.focusBorderLeft
Desktop.textfieldFocusBorderRight = Desktop.focusBorderRight
Desktop.textfieldFocusBorderTop = Desktop.focusBorderTop
Desktop.textfieldFocusBorderBottom = Desktop.focusBorderBottom
Desktop.textfieldFocusShiftLeft = 0
Desktop.textfieldFocusShiftRight = 0
Desktop.textfieldFocusShiftTop = 0
Desktop.textfieldFocusShiftBottom = 0
Desktop.textfieldFocusShiftBottom = 0
Desktop.textfieldFocusOpacity = 1


//////////////////////////////////
//      MultiLineTextField      //
//////////////////////////////////
Desktop.multiImageEnabled = "winxp_textfield_normal.png"
Desktop.multiImageFocus = "winxp_textfield_selected.png"
Desktop.multiBorderSize = 2
Desktop.multiTextBorderSize = 2
Desktop.multiFocusBorderSize = 3
Desktop.multiTextMargin = 3
Desktop.multiFontSize = Desktop.fontSize;

Desktop.multiFontColor0 = Desktop.mediumFontColor
Desktop.multiFontColor1 = Desktop.subMediumFontColor
Desktop.multiFontColor2 = Desktop.lightFontColor
Desktop.multiFontColor3 = Desktop.subLightFontColor

Desktop.multiSelectionColor = Desktop.subLightFontColor
Desktop.multiSelectedTextColor = Desktop.mediumFontColor
Desktop.multiFontFamily = Desktop.fontFamily
Desktop.multiHorizontalAlignement = Qt.AlignLeft
// Disabled ReadOnly
Desktop.multiImageDisabled = "winxp_textfield_disabled.png"
Desktop.multiImageReadOnly = "winxp_textfield_disabled.png"
Desktop.multiFontReadOnlyColor =  Desktop.subMediumFontColor
Desktop.multiFontInactiveColor =  Desktop.subLightFontColor
Desktop.multiDisabledOpacity = 0.7
Desktop.multiReadOnlyOpacity = 0.7
// PlaceholderText
Desktop.multiPlaceholderTextSize = Desktop.multiFontSize
Desktop.multiPlaceholderColor0 = Desktop.lightFontColor
Desktop.multiPlaceholderColor1 = Desktop.mediumFontColor
Desktop.multiPlaceholderColor2 = Desktop.subMediumFontColor
Desktop.multiPlaceholderColor3 = Desktop.subLightFontColor
Desktop.multiPlaceholderOpacity = 0.3
// image Borders
Desktop.multiBorderLeftSize = Desktop.multiBorderSize;
Desktop.multiBorderRightSize = Desktop.multiBorderSize;
Desktop.multiBorderTopSize = Desktop.multiBorderSize;
Desktop.multiBorderBottomSize = Desktop.multiBorderSize;
// image text Borders
Desktop.multiTextBorderLeftSize = Desktop.multiTextBorderSize;
Desktop.multiTextBorderRightSize = Desktop.multiTextBorderSize;
Desktop.multiTextBorderTopSize = Desktop.multiTextBorderSize;
Desktop.multiTextBorderBottomSize = Desktop.multiTextBorderSize;
//margins
Desktop.multiTextLeftMargin = Desktop.multiTextMargin;
Desktop.multiTextRightMargin = Desktop.multiTextMargin;
Desktop.multiTextTopMargin = Desktop.multiTextMargin;
Desktop.multiTextBottomMargin = Desktop.multiTextMargin;
// Focus
Desktop.multiFocusBorderLeft = Desktop.focusBorderLeft
Desktop.multiFocusBorderRight = Desktop.focusBorderRight
Desktop.multiFocusBorderTop = Desktop.focusBorderTop
Desktop.multiFocusBorderBottom = Desktop.focusBorderBottom
Desktop.multiFocusShiftLeft = 0
Desktop.multiFocusShiftRight = 0
Desktop.multiFocusShiftTop = 0
Desktop.multiFocusShiftBottom = 0
Desktop.multiFocusOpacity = 1

//////////////////////////////////
//          Dialog              //
//////////////////////////////////
Desktop.dialogMargins = 8
Desktop.dialogWidth = 300
Desktop.dialogHeight = 200
Desktop.faderColor = Desktop.lightFontColor
Desktop.faderOpacity = 0.5
Desktop.animationDuration = 500
Desktop.dialogHeaderHeight = 30
// Font
Desktop.dialogFontSize = Desktop.buttonFontSize
Desktop.dialogTitleFontSize = Desktop.dialogFontSize
Desktop.dialogButtonFontSize = Desktop.dialogFontSize
// Borders
Desktop.dialogHeaderLeftBorder = 8
Desktop.dialogHeaderRightBorder = 8
Desktop.dialogHeaderTopBorder = 12
Desktop.dialogHeaderBottomBorder = 4
Desktop.dialogMainLeftBorder = 8
Desktop.dialogMainRightBorder = 8
Desktop.dialogMainTopBorder = 8
Desktop.dialogMainBottomBorder = 8

//////////////////////////////////
//          Meter360            //
//////////////////////////////////
Desktop.meterScaleBigImage = "meter/meter360_scale_big.svg"
Desktop.meterScaleSmallImage = "meter/meter360_scale_small.svg"
Desktop.meterBackgroundImage = "meter/meter360_circle.svg"
Desktop.meterHandImage = "meter/meter360_hand.svg"
Desktop.meterSmallScaleRadiusFactor = 0.345
Desktop.meterBigScaleRadiusFactor = 0.345
Desktop.meterHandRadiusFactor = 0.315
Desktop.meterZoneRadiusFactor = 0.37
Desktop.meterOverlayImage = ""
Desktop.meterFontColorMiddleText = Desktop.mediumFontColor
Desktop.meterFontColorLabelText = Desktop.mediumFontColor
Desktop.meterFontPointSize = Desktop.fontSize
Desktop.meterFontBold = false
Desktop.meterValueTextPointSize = Desktop.fontSize
Desktop.meterFontFamily = Desktop.fontFamily
Desktop.meterTextVerticalOffsetFactor = 0.05
Desktop.meterScaleLabelShift = 10
Desktop.meterBigScaleWidthFactor = 0.006
Desktop.meterBigScaleHeightFactor = 0.036
Desktop.meterSmallScaleWidthFactor = 0.005
Desktop.meterSmallScaleHeightFactor = 0.02
Desktop.meterHandWidthFactor = 0.025
Desktop.meterHandHeightFactor = 0.05
Desktop.meterZonesOpacity = 0.8
Desktop.meterGreenZoneColor = "#289448"
Desktop.meterGreenZoneWidthFactor = 0.01
Desktop.meterGreenZoneShift = 0
Desktop.meterYellowZoneColor = "#F6BF00"
Desktop.meterYellowZoneWidthFactor = 0.01
Desktop.meterYellowZoneShift = 0
Desktop.meterRedZoneColor = "#C10E1A"
Desktop.meterRedZoneWidthFactor = 0.01
Desktop.meterRedZoneShift = 0

//////////////////////////////////
//          LinearScale         //
//////////////////////////////////
Desktop.linearScaleBigImage = "meter/linear_scale_big.svg"
Desktop.linearScaleSmallImage = ""
Desktop.linearBackgroundImage = "meter/linear_background.svg"
Desktop.linearYStartFactor = 0.9
Desktop.linearYStopFactor = 0.1
Desktop.linearXStartFactor = 0.07
Desktop.linearBigScaleXStart = 0.1 // Aradex.linearXStartFactor
Desktop.linearSmallScaleXStart = 0 // Aradex.linearXStartFactor
Desktop.linearHandRadiusFactor = 0.1// 0.251???
Desktop.linearVerticalLineWidthFactor = 0.014
Desktop.linearOverlayImage = ""
Desktop.linearHandImage = "meter/linear_hand.svg"
Desktop.linearFontColorMiddleText = Desktop.mediumFontColor
Desktop.linearFontColorLabelText = Desktop.mediumFontColor
Desktop.linearFontPointSize = Desktop.fontSize
Desktop.linearFontBold = false
Desktop.linearValueTextPointSize = Desktop.fontSize
Desktop.linearFontFamily = Desktop.fontFamily
Desktop.linearTextVerticalOffsetFactor = 2.65
Desktop.linearHandVerticalOffsetFactor = -1.45
Desktop.linearScaleLabelShift = 15
Desktop.linearBigScaleWidthFactor = 0.02//0.0133
Desktop.linearBigScaleHeightFactor = 0.005//0.0033
Desktop.linearSmallScaleWidthFactor = 0
Desktop.linearSmallScaleHeightFactor = 0
Desktop.linearHandWidthFactor = 0.05
Desktop.linearHandHeightFactor = 0.025



//////////////////////////////////
//          Thermometer         //
//////////////////////////////////
Desktop.thermometerFontSize = Desktop.fontSize
Desktop.thermometerFontFamily = Desktop.fontFamily
Desktop.thermometerTextVerticalOffset = 2
Desktop.thermometerTextHorizontalOffset = 32
Desktop.labelStepSize = 2
Desktop.markerShift = 140
Desktop.thermometerBigScaleWidth = 28
Desktop.thermometerBigScaleHeight = 3
Desktop.thermometerSmallScaleWidth = 12
Desktop.thermometerSmallScaleHeight = 1
Desktop.thermometerBigScaleShift = 75
Desktop.thermometerMarkerWidth =  48
Desktop.thermometerMarkerHeight = 35
Desktop.thermometerMarkerShift = 140
Desktop.thermometerSmallScaleShift = Desktop.thermometerBigScaleShift + Desktop.thermometerBigScaleWidth -Desktop.thermometerSmallScaleWidth
Desktop.thermometerLabelCenterX = 76
Desktop.thermometerLabelCenterY = 296

//////////////////////////////////
//          ErrorWidget         //
//////////////////////////////////
Desktop.errorWidgetFontSize = 2.0 * Desktop.fontSize
Desktop.errorWidgetTextFontSize = 1.2 * Desktop.fontSize
Desktop.errorWidgetFontFamily = Desktop.fontFamily
Desktop.errorWidgetFontColor = Desktop.lightFontColor
Desktop.errorwidgetBackgroundImage = "winxp_button_disabled_closed_all.png"
Desktop.errorwidgetBackgroundBorderSize = 0


Desktop.errorCounterImage = "winxp_button_pressed_closed_left.png"
Desktop.errorNumberImage = "winxp_button_pressed_closed_right.png"
Desktop.errorTextImage = ""
Desktop.errorAcknowledgeImage = ""
Desktop.errorAcknowledgeImagePressed = ""

Desktop.warningCounterImage = "winxp_button_pressed_closed_left.png"
Desktop.warningNumberImage = "winxp_button_pressed_closed_right.png"
Desktop.warningTextImage = ""
Desktop.warningAcknowledgeImage = ""
Desktop.warningAcknowledgeImagePressed = ""

Desktop.messageCounterImage = "winxp_button_pressed_closed_left.png"
Desktop.messageNumberImage = "winxp_button_pressed_closed_right.png"
Desktop.messageTextImage = ""
Desktop.messageAcknowledgeImage = ""
Desktop.messageAcknowledgeImagePressed = ""

Desktop.messageNextImage = "icons/next.svg"
Desktop.messagePreviousImage = "icons/previous.svg"
Desktop.errorwidgetErrorImage = Desktop.errorImage
Desktop.errorwidgetWarningImage = Desktop.warningImage
Desktop.errorwidgetMessageImage = Desktop.messageImage

//////////////////////////////////
//           Indicator          //
//////////////////////////////////
Desktop.indicatorColorOn = "double_punkt_gruen.png"
Desktop.indicatorColorOff = "double_punkt_rot.png"
Desktop.indicatorColor0 = "punkt_rot.png"
Desktop.indicatorColor1 = "punkt_gruen.png"
Desktop.indicatorColor2 = "punkt_normal.png"
Desktop.indicatorColor3 = "punkt_aktiv.png"

//////////////////////////////////
//        NUMERIC INPUT         //
//////////////////////////////////
// Additional Images for non symmetrical Styles
Desktop.numericinputBackgroundImageNormal = "winxp_textfield_dark_closed_all.png"
Desktop.numericinputTextfieldImageEnabled = "winxp_textfield_dark_closed_all.png"
Desktop.numericinputTextfieldImageFocus = "winxp_textfield_dark_closed_all.png"
Desktop.numericinputTextfieldImageDisabled = "winxp_textfield_dark_closed_all.png"
Desktop.numericinputTextfieldImageReadOnly = "winxp_textfield_dark_closed_all.png"
Desktop.numericinputButtonLeftImageNormal = "winxp_button_normal_closed_right.png"
Desktop.numericinputButtonLeftImagePressed = "winxp_button_pressed_closed_right.png"
Desktop.numericinputButtonLeftImageDisabled = "winxp_button_disabled_closed_right.png"
Desktop.numericinputButtonRightImageNormal = "winxp_button_normal_closed_left.png"
Desktop.numericinputButtonRightImagePressed = "winxp_button_pressed_closed_left.png"
Desktop.numericinputButtonRightImageDisabled = "winxp_button_disabled_closed_left.png"
Desktop.numericinputButtonPlus = "plus.png"
Desktop.numericinputButtonMinus = "minus.png"
Desktop.numericinputButtonPlusMinusHeight = 16
Desktop.numericinputButtonPlusMinusWidth = 16
Desktop.numericinputButtonPlusMinusHorizontalAlignment = Qt.AlignHCenter
Desktop.numericinputButtonPlusMinusVerticalAlignment = Qt.AlignVCenter

// font
Desktop.numericinputFontSizeMiddle = Desktop.fontSize;
Desktop.numericinputFontColorMiddle = Desktop.mediumFontColor
Desktop.numericinputFontSizeLimits = Desktop.fontSize / 2
Desktop.numericinputFontColorLimits = Desktop.subMediumFontColor

//////////////////////////////////
//        Browser               //
//////////////////////////////////

Desktop.browserNextImage = "icons/next.svg"
Desktop.browserBackImage = "icons/previous.svg"
Desktop.browserReloadImage = "icons/view-refresh.png"
Desktop.browserUrlInput = "winxp_textfield_normal.png"
Desktop.browserUrlInputSelected = "winxp_textfield_selected.png"
Desktop.browserHeaderBackground = "winxp_button_normal.png"
Desktop.browserZoomInImage = "icons/ZoomIn.svg"
Desktop.browserZoomOutImage = "icons/ZoomOut.svg"
Desktop.browserBorderSize = 7;
// Image Borders
Desktop.browserBorderLeftSize = Desktop.browserBorderSize;
Desktop.browserBorderRightSize = Desktop.browserBorderSize;
Desktop.browserBorderTopSize = Desktop.browserBorderSize;
Desktop.browserBorderBottomSize = Desktop.browserBorderSize;

//////////////////////////////////
//      FileChooser             //
//////////////////////////////////
Desktop.fileChooserSubcomponentBorderWidth = 4
Desktop.fileChooserBackImage  = "icons/moveUp.png"
Desktop.fileChooserFolderImage  = "icons/folder.png"
Desktop.fileChooserFileImage  = "icons/file.png"
Desktop.fileChooserErrorImage = Desktop.errorImage

////////////////////////////////
//      Progress Bar          //
////////////////////////////////
Desktop.progressBarBackground = "progressbar_background.png"
Desktop.progressBarFilling = "progressbar_filling.png"
Desktop.progressBarBackgroundBorder = 2
Desktop.progressBarBackgroundBorderLeft = Desktop.progressBarBackgroundBorder
Desktop.progressBarBackgroundBorderRight = Desktop.progressBarBackgroundBorder
Desktop.progressBarBackgroundBorderTop = Desktop.progressBarBackgroundBorder
Desktop.progressBarBackgroundBorderBottom = Desktop.progressBarBackgroundBorder
Desktop.progressBarFillingX = 1
Desktop.progressBarFillingY = 1
Desktop.progressBarFillingSourceWidth = 1
Desktop.progressBarFillingSourceHeight = 1
Desktop.progressBarTextBold = false
Desktop.progressBarTextColorChoice = 0
Desktop.progressBarTextOutline= false
Desktop.progressBarTextOutlineColor = "#000000"
Desktop.progressBarFillingInBackground = true

//////////////////////////////////
//             	 LOGVIEW        //
//////////////////////////////////
Desktop.logFontColor0 = Desktop.lightFontColor
Desktop.logFontColor1 = Desktop.mediumFontColor
Desktop.logFontColor2 = Desktop.lightColor
Desktop.logFontColor3 = Desktop.mediumColor
Desktop.logFontSize = Desktop.fontSize
Desktop.logFontFamily = Desktop.fontFamily
