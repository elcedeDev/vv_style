// $Revision: 21117 $
.pragma library

var Aradex = new Object();


//////////////////////////////////
//            GENERAL           //
//////////////////////////////////
Aradex.imageFolder = Qt.resolvedUrl("images/");

Aradex.fontFamily = "Arial"
Aradex.fontFamilyBold = "Arial Black"
Aradex.fontSize = 14
Aradex.backgroundColor = "#848484"
Aradex.lightColor = "#95C9E4"
Aradex.mediumColor = "#1C86B7"
Aradex.lightFontColor = "#EBEBEB"
Aradex.subLightFontColor = "#AAAAAA"
Aradex.mediumFontColor = "#3E3E3E"
Aradex.subMediumFontColor = "#4C4C4C"
Aradex.errorImage = "icons/Error.svg"
Aradex.warningImage = "icons/Warning.svg"
Aradex.messageImage = "icons/Message.svg"

//////////////////////////////////
//            FOCUS             //
//////////////////////////////////
Aradex.focusBorderLeft = 7
Aradex.focusBorderRight = 7
Aradex.focusBorderTop = 7
Aradex.focusBorderBottom = 7
Aradex.focusShiftLeft = -1
Aradex.focusShiftRight = -1
Aradex.focusShiftTop = -1
Aradex.focusShiftBottom = -1
Aradex.focusOpacity = 1
Aradex.focusImage = ""

//////////////////////////////////
//            THEME             //
//////////////////////////////////
Aradex.themeImage0 = "background_hell_grau.png"
Aradex.themeImage1 = "background_hell_blau.png"
Aradex.themeImage2 = "background_dunkel_grau.png"
Aradex.themeImage3 = "background_dunkel_blau.png"
Aradex.themeImage4 = "background_rot.png"
Aradex.themeImage5 = ""
Aradex.themeImage6 = ""
Aradex.themeImage7 = ""
Aradex.themeColor0 = "#EBEBEB"
Aradex.themeColor1 = "#95C9E4"
Aradex.themeColor2 = "#3E3E3E"
Aradex.themeColor3 = "#1C86B7"
Aradex.themeColor4 = "#C10E1A"
Aradex.themeColor5 = ""
Aradex.themeColor6 = ""
Aradex.themeColor7 = ""
Aradex.themeHasImage = true
Aradex.themeBorder = 7
Aradex.themeBorderLeft = Aradex.themeBorder
Aradex.themeBorderRight = Aradex.themeBorder
Aradex.themeBorderTop = Aradex.themeBorder
Aradex.themeBorderBottom = Aradex.themeBorder

//////////////////////////////////
//       VIRTUAL KEYBOARD       //
//////////////////////////////////
Aradex.virtualKeyboardBackgroundColor = "black"
Aradex.virtualKeyboardKeyBackgroundColor = "dimgrey"
Aradex.virtualKeyboardKeyTextColor = "white"
Aradex.virtualKeyboardControlKeyBackgroundColor = "#353535" // a dark grey
Aradex.virtualKeyboardControlKeyTextColor = "white"
Aradex.virtualKeyboardControlKeyTextActiveColor = "black"

//////////////////////////////////
//          CONTAINER           //
//////////////////////////////////
// Colors correspond to Theme Colors above
// [main themeshape colorchoice, header themeshape colorchoice, fontcolor]
Aradex.containerColors0 = [1,3,2]
Aradex.containerColors1 = [2,1,1]
Aradex.containerColors2 = [0,1,3]
Aradex.containerColors3 = [4,0,2]
Aradex.containerHeaderHeight = 32
Aradex.containerHeaderWidth = 120
Aradex.containerHeaderVisible = true
Aradex.containerHeaderTextInBorder = false
Aradex.containerFitText = true
Aradex.containerTextXShift = Aradex.fontSize
Aradex.containerTextYShift = Aradex.fontSize


//////////////////////////////////
//           Segment            //
//////////////////////////////////
Aradex.segmentImage0 = "trenner_hell_grau.png"
Aradex.segmentImage1 = "trenner_hell_blau.png"
Aradex.segmentImage2 = "trenner_dunkel_grau.png"
Aradex.segmentImage3 = "trenner_dunkel_blau.png"
Aradex.segmentImage4 = "trenner_rot.png"
Aradex.segmentImage5 = ""
Aradex.segmentImage6 = ""
Aradex.segmentImage7 = ""
Aradex.segmentImageHeight = 40
Aradex.segmentBorderLeft = 40
Aradex.segmentBorderRight = 6
Aradex.segmentBorderTop = 32
Aradex.segmentBorderBottom = 0

Aradex.segmentHeight = 200
Aradex.segmentWidth = 120

//////////////////////////////////
//          LABELLIST           //
//////////////////////////////////
Aradex.labellistColor0 = Aradex.themeColor0
Aradex.labellistColor1 = Aradex.themeColor1
Aradex.labellistColor2 = Aradex.themeColor2
Aradex.labellistColor3 = Aradex.themeColor3
Aradex.labellistColor4 = Aradex.themeColor4
Aradex.labellistLeftRightMargin = 4
Aradex.labellistSeperatorWidth = 1



//////////////////////////////////
//            BUTTON            //
//////////////////////////////////
Aradex.buttonImageNormal = "background_btn_normal_normal.png"
Aradex.buttonImagePressed = "background_btn_normal_normal.png"
Aradex.buttonImageDisabled = "background_btn_normal_normal.png"
Aradex.buttonImageFocus = "background_btn_normal_focus.png"
Aradex.buttonBorderSize = 7;
Aradex.buttonTextBorderSize = 3
Aradex.buttonFontSize = Aradex.fontSize
Aradex.buttonTextMargin = 3;
Aradex.buttonFontColor = Aradex.mediumFontColor
Aradex.buttonFontPressedColor = Aradex.mediumColor
Aradex.buttonFontDisabledColor = Aradex.subLightFontColor
Aradex.buttonFontFamily = Aradex.fontFamily
// Image Borders
Aradex.buttonBorderLeftSize = Aradex.buttonBorderSize;
Aradex.buttonBorderRightSize = Aradex.buttonBorderSize;
Aradex.buttonBorderTopSize = Aradex.buttonBorderSize;
Aradex.buttonBorderBottomSize = Aradex.buttonBorderSize;

// Text Borders
Aradex.buttonTextBorderLeftSize = Aradex.buttonTextBorderSize;
Aradex.buttonTextBorderRightSize = Aradex.buttonTextBorderSize;
Aradex.buttonTextBorderTopSize = Aradex.buttonTextBorderSize;
Aradex.buttonTextBorderBottomSize = Aradex.buttonTextBorderSize;

//margins
Aradex.buttonTextLeftMargin = 5;
Aradex.buttonTextRightMargin = 0;
Aradex.buttonTextTopMargin = 5;
Aradex.buttonTextBottomMargin = 0;
// text Alignement
Aradex.buttonHorizontalAlignement = Qt.AlignLeft
Aradex.buttonVerticalAlignement = Qt.AlignBottom
// Button Shift when Pressed
Aradex.buttonPressedLeftShift = -1
Aradex.buttonPressedRightShift = -1
Aradex.buttonPressedTopShift = 0
Aradex.buttonPressedBottomShift = -1
// Animations
Aradex.buttonSizeAnimationTime = 0
Aradex.buttonTextAnimationTime = 0
// Focus
Aradex.buttonFocusBorderLeft = Aradex.focusBorderLeft
Aradex.buttonFocusBorderRight = Aradex.focusBorderRight
Aradex.buttonFocusBorderTop = Aradex.focusBorderTop
Aradex.buttonFocusBorderBottom = Aradex.focusBorderBottom
Aradex.buttonFocusShiftLeft = Aradex.focusShiftLeft
Aradex.buttonFocusShiftRight = Aradex.focusShiftRight
Aradex.buttonFocusShiftTop = Aradex.focusShiftTop
Aradex.buttonFocusShiftBottom = Aradex.focusShiftBottom
// forground button
Aradex.buttonForegroundImageHeight = 32
Aradex.buttonForegroundImageWidth = 32

//////////////////////////////////
//       LANGUAGE BUTTON        //
//////////////////////////////////

Aradex.languageButtonImage = new Object()
Aradex.languageButtonImage["en"] = "lang/United-Kingdom-64.png"
Aradex.languageButtonImage["de"] = "lang/Germany-64.png"
Aradex.languageButtonImage["fr"] = "lang/France-64.png"
Aradex.languageButtonImage["sp"] = "lang/Spain-64.png"
Aradex.languageButtonImage["zh"] = "lang/China-64.png"
Aradex.languagebuttonImageHeight = 48
Aradex.languagebuttonImageWidth = 48
Aradex.languagebuttonImageHorizontalAlignement = Qt.AlignRight
Aradex.languagebuttonImageVerticalAlignement = Qt.AlignVCenter
Aradex.languagebuttonImageXShift = - 16
Aradex.languagebuttonImageYShift = 0

//////////////////////////////////
//         logFile BUTTON        //
//////////////////////////////////
Aradex.logFileButtonImage= "icons/logFile.svg"
Aradex.logFileButtonImageHeight = 48
Aradex.logFileButtonImageWidth = 48
Aradex.logFileButtonImageHorizontalAlignement = Qt.AlignHCenter
Aradex.logFileButtonImageVerticalAlignement = Qt.AlignVCenter
Aradex.logFileButtonImageXShift = 0
Aradex.logFileButtonImageYShift = 0

//////////////////////////////////
//         STYLE  BUTTON        //
//////////////////////////////////
Aradex.styleButtonImage = new Object()
Aradex.styleButtonImage["Aradex"] = ""
Aradex.styleButtonImage["WinXP"] = ""
Aradex.stylebuttonImageHeight = 0
Aradex.stylebuttonImageWidth = 0
Aradex.stylebuttonImageHorizontalAlignement = Qt.AlignRight
Aradex.stylebuttonImageVerticalAlignement = Qt.AlignVCenter
Aradex.stylebuttonImageXShift = 0
Aradex.stylebuttonImageYShift = 0

//////////////////////////////////
//            TOGGLEBUTTON      //
//////////////////////////////////
Aradex.togglebuttonImageNormal = "background_btn_normal_normal.png"
Aradex.togglebuttonImagePressed = "background_btn_normal_normal.png"
Aradex.togglebuttonImageDisabled = "background_btn_normal_normal.png"
Aradex.togglebuttonImagePressedAndDisabled = "background_btn_normal_normal.png"
Aradex.togglebuttonImageFocus = Aradex.focusImage
Aradex.togglebuttonLightImageOff = "punkt_normal.png"
Aradex.togglebuttonLightImageOn = "punkt_aktiv.png"
Aradex.togglebuttonLightHorizontalAlignement = Qt.AlignRight
Aradex.togglebuttonLightVerticalAlignement = Qt.AlignTop
Aradex.togglebuttonLightImageHeight = 30
Aradex.togglebuttonLightImageWidth = 30

Aradex.togglebuttonBorderSize = 7;
Aradex.togglebuttonTextBorderSize = 3
Aradex.togglebuttonFontSize = Aradex.fontSize
Aradex.togglebuttonTextMargin = 3;
Aradex.togglebuttonFontColor = Aradex.mediumFontColor
Aradex.togglebuttonFontPressedColor = Aradex.mediumFontColor
Aradex.togglebuttonFontDisabledColor = Aradex.lightColor
Aradex.togglebuttonFontPressedAndDisabledColor = Aradex.lightColor
Aradex.togglebuttonFocusBorderColor = "#00000000"
Aradex.togglebuttonFontFamily = Aradex.fontFamily
// Image Borders
Aradex.togglebuttonBorderLeftSize = Aradex.togglebuttonBorderSize;
Aradex.togglebuttonBorderRightSize = Aradex.togglebuttonBorderSize;
Aradex.togglebuttonBorderTopSize = Aradex.togglebuttonBorderSize;
Aradex.togglebuttonBorderBottomSize = Aradex.togglebuttonBorderSize;

// Text Borders
Aradex.togglebuttonTextBorderLeftSize = Aradex.togglebuttonTextBorderSize;
Aradex.togglebuttonTextBorderRightSize = Aradex.togglebuttonTextBorderSize;
Aradex.togglebuttonTextBorderTopSize = Aradex.togglebuttonTextBorderSize;
Aradex.togglebuttonTextBorderBottomSize = Aradex.togglebuttonTextBorderSize;

//margins
Aradex.togglebuttonTextLeftMargin = 5;
Aradex.togglebuttonTextRightMargin = 0;
Aradex.togglebuttonTextTopMargin = 5;
Aradex.togglebuttonTextBottomMargin = 0;
// text Alignement
Aradex.togglebuttonHorizontalAlignement = Qt.AlignLeft
Aradex.togglebuttonVerticalAlignement = Qt.AlignBottom

// Button Shift when Pressed
Aradex.togglebuttonPressedLeftShift = -2
Aradex.togglebuttonPressedRightShift = -2
Aradex.togglebuttonPressedTopShift = -2
Aradex.togglebuttonPressedBottomShift = -2
// Animations
Aradex.togglebuttonSizeAnimationTime = 0
Aradex.togglebuttonTextAnimationTime = 0
// Focus
Aradex.togglebuttonFocusBorderLeft = Aradex.focusBorderLeft
Aradex.togglebuttonFocusBorderRight = Aradex.focusBorderRight
Aradex.togglebuttonFocusBorderTop = Aradex.focusBorderTop
Aradex.togglebuttonFocusBorderBottom = Aradex.focusBorderBottom
Aradex.togglebuttonFocusShiftLeft = Aradex.focusShiftLeft
Aradex.togglebuttonFocusShiftRight = Aradex.focusShiftRight
Aradex.togglebuttonFocusShiftTop = Aradex.focusShiftTop
Aradex.togglebuttonFocusShiftBottom = Aradex.focusShiftBottom
// forground button
Aradex.togglebuttonForegroundImageHeight = 32
Aradex.togglebuttonForegroundImageWidth = 32

//////////////////////////////////
//          RADIOBUTTON         //
//////////////////////////////////
Aradex.radiobuttonImageNormal = "background_btn_normal_normal.png"
Aradex.radiobuttonImagePressed = "background_btn_normal_normal.png"
Aradex.radiobuttonImageDisabled = "background_btn_normal_normal.png"
Aradex.radiobuttonImagePressedAndDisabled = "background_btn_normal_normal.png"
Aradex.radiobuttonImageFocus = Aradex.focusImage
Aradex.radiobuttonLightImageOff = "punkt_normal.png"
Aradex.radiobuttonLightImageOn = "punkt_aktiv.png"
Aradex.radiobuttonLightImageHeight = 30
Aradex.radiobuttonLightImageWidth = 30
Aradex.radiobuttonLightHorizontalAlignement = Qt.AlignRight
Aradex.radiobuttonLightVerticalAlignement = Qt.AlignTop
Aradex.radiobuttonBorderSize = 7;
Aradex.radiobuttonTextBorderSize = 3
Aradex.radiobuttonFontSize = Aradex.fontSize
Aradex.radiobuttonTextMargin = 3;
Aradex.radiobuttonFontColor = Aradex.mediumFontColor
Aradex.radiobuttonFontPressedColor = Aradex.mediumColor
Aradex.radiobuttonFontDisabledColor = Aradex.lightColor
Aradex.radiobuttonFontPressedAndDisabledColor = Aradex.lightColor
Aradex.radiobuttonFocusBorderColor = "#00000000"
Aradex.radiobuttonFontFamily = Aradex.fontFamily
// Image Borders
Aradex.radiobuttonBorderLeftSize = Aradex.radiobuttonBorderSize;
Aradex.radiobuttonBorderRightSize = Aradex.radiobuttonBorderSize;
Aradex.radiobuttonBorderTopSize = Aradex.radiobuttonBorderSize;
Aradex.radiobuttonBorderBottomSize = Aradex.radiobuttonBorderSize;
// Text Borders
Aradex.radiobuttonTextBorderLeftSize = Aradex.radiobuttonTextBorderSize;
Aradex.radiobuttonTextBorderRightSize = Aradex.radiobuttonTextBorderSize;
Aradex.radiobuttonTextBorderTopSize = Aradex.radiobuttonTextBorderSize;
Aradex.radiobuttonTextBorderBottomSize = Aradex.radiobuttonTextBorderSize;

//margins
Aradex.radiobuttonTextLeftMargin = 5;
Aradex.radiobuttonTextRightMargin = 0;
Aradex.radiobuttonTextTopMargin = 5;
Aradex.radiobuttonTextBottomMargin = 0;
// text Alignement
Aradex.radiobuttonHorizontalAlignement = Qt.AlignLeft
Aradex.radiobuttonVerticalAlignement = Qt.AlignBottom

// Button Shift when Pressed
Aradex.radiobuttonPressedLeftShift = -1
Aradex.radiobuttonPressedRightShift = -1
Aradex.radiobuttonPressedTopShift = -1
Aradex.radiobuttonPressedBottomShift = -1
// Animations
Aradex.radiobuttonSizeAnimationTime = 0
Aradex.radiobuttonTextAnimationTime = 0
// Focus
Aradex.radiobuttonFocusBorderLeft = Aradex.focusBorderLeft
Aradex.radiobuttonFocusBorderRight = Aradex.focusBorderRight
Aradex.radiobuttonFocusBorderTop = Aradex.focusBorderTop
Aradex.radiobuttonFocusBorderBottom = Aradex.focusBorderBottom
Aradex.radiobuttonFocusShiftLeft = Aradex.focusShiftLeft
Aradex.radiobuttonFocusShiftRight = Aradex.focusShiftRight
Aradex.radiobuttonFocusShiftTop = Aradex.focusShiftTop
Aradex.radiobuttonFocusShiftBottom = Aradex.focusShiftBottom

//////////////////////////////////
//             	   LABEL        //
//////////////////////////////////
Aradex.labelImage0 = ""
Aradex.labelImage1 = "background_hell_grau.png"
Aradex.labelImage2 = "background_hell_blau.png"
Aradex.labelImage3 = "background_dunkel_blau.png"
Aradex.labelFontColor0 = Aradex.lightFontColor
Aradex.labelFontColor1 = Aradex.mediumFontColor
Aradex.labelFontColor2 = Aradex.lightColor
Aradex.labelFontColor3 = Aradex.mediumColor
Aradex.labelBorderSize = 7;
Aradex.labelTextBorderSize = 2
Aradex.labelFontSize = Aradex.fontSize
Aradex.labelTextMargin = 3;
Aradex.labelFontColor = "black"
Aradex.labelFocusBorderColor = "#00000000"
// text Alignement
Aradex.labelHorizontalAlignement = Qt.AlignLeft
Aradex.labelVerticalAlignement = Qt.AlignBottom
Aradex.labelFontFamily = Aradex.fontFamily
// image borders
Aradex.labelTextBorderLeftSize = Aradex.labelTextBorderSize
Aradex.labelTextBorderRightSize = Aradex.labelTextBorderSize
Aradex.labelTextBorderTopSize = Aradex.labelTextBorderSize
Aradex.labelTextBorderBottomSize = Aradex.labelTextBorderSize
// text Borders
Aradex.labelBorderLeftSize = Aradex.labelBorderSize
Aradex.labelBorderRightSize = Aradex.labelBorderSize
Aradex.labelBorderTopSize = Aradex.labelBorderSize
Aradex.labelBorderBottomSize = Aradex.labelBorderSize
// margins
Aradex.labelTextLeftMargin = Aradex.labelTextMargin
Aradex.labelTextRightMargin = 0//Aradex.labelTextMargin
Aradex.labelTextTopMargin = 0//Aradex.labelTextMargin
Aradex.labelTextBottomMargin = Aradex.labelTextMargin
// animation
Aradex.labelAnimationHighlightColor = Aradex.lightColor
Aradex.labelAnimationHighlightHoldTime = 1000
Aradex.labelAnimationHighlightTransitionTime = 2500


//////////////////////////////////
//          TextField           //
//////////////////////////////////
Aradex.textfieldImageEnabled = "textfield_background_normal.svg"
Aradex.textfieldImageFocus = "textfield_background_selected.svg"
Aradex.textfieldBorderSize = 7
Aradex.textfieldFocusBorderSize = 7
Aradex.textfieldTextBorderSize = 7
Aradex.textfieldFontSize = Aradex.fontSize;

Aradex.fontColor0 = Aradex.lightFontColor
Aradex.fontColor1 = Aradex.mediumFontColor
Aradex.fontColor2 = Aradex.lightColor
Aradex.fontColor3 = Aradex.mediumColor

Aradex.textfieldSelectionColor = Aradex.mediumFontColor
Aradex.textfieldSelectedTextColor = Aradex.lightFontColor
Aradex.textfieldFontFamily = Aradex.fontFamily
Aradex.textfieldHorizontalAlignement = Qt.AlignLeft
// Disabled - read-only
Aradex.textfieldImageDisabled = "textfield_background_normal.svg"
Aradex.textfieldImageReadOnly = "textfield_background_normal.svg"
Aradex.textfieldFontReadOnlyColor = Aradex.lightFontColor
Aradex.textfieldFontInactiveColor = Aradex.subMediumFontColor
Aradex.textfieldDisabledOpacity = 0.7
Aradex.textfieldReadOnlyOpacity = 0.7
// Placeholder Text
Aradex.textfieldPlaceholderTextSize = Aradex.textfieldFontSize * 0.7
Aradex.textfieldFontPlaceholderColor = Aradex.lightFontColor
Aradex.textfieldPlaceholderOpacity = 0.3

// image Borders
Aradex.textfieldBorderLeftSize = Aradex.textfieldBorderSize;
Aradex.textfieldBorderRightSize = Aradex.textfieldBorderSize;
Aradex.textfieldBorderTopSize = Aradex.textfieldBorderSize;
Aradex.textfieldBorderBottomSize = Aradex.textfieldBorderSize;

// image text Borders
Aradex.textfieldTextBorderLeftSize = 7//Aradex.textfieldTextBorderSize;
Aradex.textfieldTextBorderRightSize = 4//Aradex.textfieldTextBorderSize;
Aradex.textfieldTextBorderTopSize = 4//Aradex.textfieldTextBorderSize;
Aradex.textfieldTextBorderBottomSize = 4//Aradex.textfieldTextBorderSize;

// Focus
Aradex.textfieldFocusBorderLeft = Aradex.focusBorderLeft
Aradex.textfieldFocusBorderRight = Aradex.focusBorderRight
Aradex.textfieldFocusBorderTop = Aradex.focusBorderTop
Aradex.textfieldFocusBorderBottom = Aradex.focusBorderBottom
Aradex.textfieldFocusShiftLeft = 0
Aradex.textfieldFocusShiftRight = 0
Aradex.textfieldFocusShiftTop = 0
Aradex.textfieldFocusShiftBottom = 0
Aradex.textfieldFocusShiftBottom = 0
Aradex.textfieldFocusOpacity = 1


//////////////////////////////////
//      MultiLineTextField      //
//////////////////////////////////
Aradex.multiImageEnabled = "textfield_background_normal.svg"
Aradex.multiImageFocus = "textfield_background_selected.svg"
Aradex.multiBorderSize = 7
Aradex.multiTextBorderSize = 3
Aradex.multiFocusBorderSize = 7
Aradex.multiTextMargin = 3
Aradex.multiFontSize = Aradex.fontSize;

Aradex.multiFontColor0 = Aradex.lightFontColor
Aradex.multiFontColor1 = Aradex.mediumFontColor
Aradex.multiFontColor2 = Aradex.lightColor
Aradex.multiFontColor3 = Aradex.mediumColor

Aradex.multiSelectionColor = Aradex.mediumFontColor
Aradex.multiSelectedTextColor = Aradex.lightFontColor
Aradex.multiFontFamily = Aradex.fontFamily
Aradex.multiHorizontalAlignement = Qt.AlignLeft
// Disabled ReadOnly
Aradex.multiImageDisabled = "background_textfield_normal.png"
Aradex.multiImageReadOnly = "background_textfield_normal.png"
Aradex.multiFontReadOnlyColor =  Aradex.subMediumFontColor
Aradex.multiFontInactiveColor =  Aradex.subMediumFontColor
Aradex.multiDisabledOpacity = 0.7
Aradex.multiReadOnlyOpacity = 0.7
// PlaceholderText
Aradex.multiPlaceholderTextSize = Aradex.multiFontSize * 0.7;
Aradex.multiPlaceholderColor0 = Aradex.lightFontColor
Aradex.multiPlaceholderColor1 = Aradex.mediumFontColor
Aradex.multiPlaceholderColor2 = Aradex.lightColor
Aradex.multiPlaceholderColor3 = Aradex.mediumColor
Aradex.multiPlaceholderOpacity = 0.3

// image Borders
Aradex.multiBorderLeftSize = Aradex.multiBorderSize;
Aradex.multiBorderRightSize = Aradex.multiBorderSize;
Aradex.multiBorderTopSize = Aradex.multiBorderSize;
Aradex.multiBorderBottomSize = Aradex.multiBorderSize;
// image text Borders
Aradex.multiTextBorderLeftSize = Aradex.multiTextBorderSize;
Aradex.multiTextBorderRightSize = Aradex.multiTextBorderSize;
Aradex.multiTextBorderTopSize = Aradex.multiTextBorderSize;
Aradex.multiTextBorderBottomSize = Aradex.multiTextBorderSize;
//margins
Aradex.multiTextLeftMargin = Aradex.multiTextMargin;
Aradex.multiTextRightMargin = Aradex.multiTextMargin;
Aradex.multiTextTopMargin = Aradex.multiTextMargin;
Aradex.multiTextBottomMargin = Aradex.multiTextMargin;
// Focus
Aradex.multiFocusBorderLeft = Aradex.focusBorderLeft
Aradex.multiFocusBorderRight = Aradex.focusBorderRight
Aradex.multiFocusBorderTop = Aradex.focusBorderTop
Aradex.multiFocusBorderBottom = Aradex.focusBorderBottom
Aradex.multiFocusShiftLeft = 0
Aradex.multiFocusShiftRight = 0
Aradex.multiFocusShiftTop = 0
Aradex.multiFocusShiftBottom = 0
Aradex.multiFocusOpacity = 1

//////////////////////////////////
//          Dialog              //
//////////////////////////////////
Aradex.dialogMargins = 8
Aradex.dialogWidth = 360
Aradex.dialogHeight = 200
Aradex.faderColor = Aradex.backgroundColor
Aradex.faderOpacity = 0.5
Aradex.animationDuration = 500
Aradex.dialogHeaderHeight = 30
// Font
Aradex.dialogFontSize = Aradex.buttonFontSize
Aradex.dialogTitleFontSize = Aradex.dialogFontSize
Aradex.dialogButtonFontSize = Aradex.dialogFontSize
// Borders
Aradex.dialogHeaderLeftBorder = 8
Aradex.dialogHeaderRightBorder = 8
Aradex.dialogHeaderTopBorder = 12
Aradex.dialogHeaderBottomBorder = 4
Aradex.dialogMainLeftBorder = 8
Aradex.dialogMainRightBorder = 8
Aradex.dialogMainTopBorder = 8
Aradex.dialogMainBottomBorder = 8

//////////////////////////////////
//          Meter360            //
//////////////////////////////////
Aradex.meterScaleBigImage = "meter/meter360_scale_big.svg"
Aradex.meterScaleSmallImage = "meter/meter360_scale_small.svg"
Aradex.meterBackgroundImage = "meter/meter360_circle.svg"
Aradex.meterHandImage = "meter/meter360_hand.svg"
Aradex.meterSmallScaleRadiusFactor = 0.27
Aradex.meterBigScaleRadiusFactor = 0.27
Aradex.meterHandRadiusFactor = 0.24
Aradex.meterZoneRadiusFactor = 0.31
Aradex.meterOverlayImage = ""
Aradex.meterFontColorMiddleText = Aradex.lightColor
Aradex.meterFontColorLabelText = Aradex.lightColor
Aradex.meterFontPointSize = Aradex.fontSize
Aradex.meterFontBold = false
Aradex.meterValueTextPointSize = Aradex.fontSize
Aradex.meterFontFamily = Aradex.fontFamily
Aradex.meterTextVerticalOffsetFactor = 0.05
Aradex.meterScaleLabelShift = 10
Aradex.meterBigScaleWidthFactor = 0.01
Aradex.meterBigScaleHeightFactor = 0.11
Aradex.meterSmallScaleWidthFactor = 0.005
Aradex.meterSmallScaleHeightFactor = 0.07
Aradex.meterHandWidthFactor = 0.04
Aradex.meterHandHeightFactor = 0.16
Aradex.meterZonesOpacity = 0.8
Aradex.meterGreenZoneColor = "#289448"
Aradex.meterGreenZoneWidthFactor = 0.02
Aradex.meterGreenZoneShift = 0
Aradex.meterYellowZoneColor = "#F6BF00"
Aradex.meterYellowZoneWidthFactor = 0.02
Aradex.meterYellowZoneShift = 0
Aradex.meterRedZoneColor = "#C10E1A"
Aradex.meterRedZoneWidthFactor = 0.02
Aradex.meterRedZoneShift = 0

//////////////////////////////////
//          LinearScale         //
//////////////////////////////////
Aradex.linearScaleBigImage = "meter/linear_scale_big.svg"
Aradex.linearScaleSmallImage = "meter/linear_scale_small.svg"
Aradex.linearBackgroundImage = "meter/linear_scale_big.svg"
Aradex.linearYStartFactor = 0.9
Aradex.linearYStopFactor = 0.1
Aradex.linearXStartFactor = 0.07
Aradex.linearBigScaleXStart = Aradex.linearXStartFactor
Aradex.linearSmallScaleXStart = Aradex.linearXStartFactor
Aradex.linearHandRadiusFactor = 0.251
Aradex.linearVerticalLineWidthFactor = 0.01
Aradex.linearOverlayImage = ""
Aradex.linearHandImage = "meter/linear_hand.svg"
Aradex.linearFontColorMiddleText = Aradex.lightColor
Aradex.linearFontColorLabelText = Aradex.lightColor
Aradex.linearFontPointSize = Aradex.fontSize
Aradex.linearFontBold = false
Aradex.linearValueTextPointSize = Aradex.fontSize
Aradex.linearFontFamily = Aradex.fontFamily
Aradex.linearTextVerticalOffsetFactor = 0.65
Aradex.linearHandVerticalOffsetFactor = 0.6
Aradex.linearScaleLabelShift = 10
Aradex.linearBigScaleWidthFactor = 0.11
Aradex.linearBigScaleHeightFactor = 0.009
Aradex.linearSmallScaleWidthFactor = 0.07
Aradex.linearSmallScaleHeightFactor = 0.005
Aradex.linearHandWidthFactor = 0.06
Aradex.linearHandHeightFactor = 0.048



//////////////////////////////////
//          Thermometer         //
//////////////////////////////////
Aradex.thermometerFontSize = Aradex.fontSize
Aradex.thermometerFontFamily = Aradex.fontFamily
Aradex.thermometerTextVerticalOffset = 2
Aradex.thermometerTextHorizontalOffset = 32
Aradex.labelStepSize = 2
Aradex.markerShift = 140
Aradex.thermometerBigScaleWidth = 28
Aradex.thermometerBigScaleHeight = 3
Aradex.thermometerSmallScaleWidth = 12
Aradex.thermometerSmallScaleHeight = 1
Aradex.thermometerBigScaleShift = 75
Aradex.thermometerMarkerWidth =  48
Aradex.thermometerMarkerHeight = 35
Aradex.thermometerMarkerShift = 140
Aradex.thermometerSmallScaleShift = Aradex.thermometerBigScaleShift + Aradex.thermometerBigScaleWidth -Aradex.thermometerSmallScaleWidth
Aradex.thermometerLabelCenterX = 76
Aradex.thermometerLabelCenterY = 296

/////////////////////////////////
//          ErrorWidget                //
/////////////////////////////////
Aradex.errorWidgetFontSize = 2.0 * Aradex.fontSize
Aradex.errorWidgetTextFontSize = 1.2 * Aradex.fontSize
Aradex.errorWidgetFontFamily = Aradex.fontFamily
Aradex.errorWidgetFontColor = Aradex.lightFontColor
Aradex.errorwidgetBackgroundImage = "background_dunkel_grau.png"
Aradex.errorwidgetBackgroundBorderSize = 7


Aradex.errorCounterImage = "bg_error_links.png"
Aradex.errorNumberImage = "bg_error_rechts.png"
Aradex.errorTextImage = ""
Aradex.errorAcknowledgeImage = "background_btn_check_normal.png"
Aradex.errorAcknowledgeImagePressed = "background_btn_check_aktive.png"

Aradex.warningCounterImage = "bg_warning_links.png"
Aradex.warningNumberImage = "bg_warning_rechts.png"
Aradex.warningTextImage = ""
Aradex.warningAcknowledgeImage = "background_btn_check_normal.png"
Aradex.warningAcknowledgeImagePressed = "background_btn_check_aktive.png"

Aradex.messageCounterImage = "bg_status_links.png"
Aradex.messageNumberImage = "bg_status_rechts.png"
Aradex.messageTextImage = ""
Aradex.messageAcknowledgeImage = "background_btn_check_normal.png"
Aradex.messageAcknowledgeImagePressed = "background_btn_check_aktive.png"

Aradex.messageNextImage = "icons/next.svg"
Aradex.messagePreviousImage = "icons/previous.svg"
Aradex.errorwidgetErrorImage = Aradex.errorImage
Aradex.errorwidgetWarningImage = Aradex.warningImage
Aradex.errorwidgetMessageImage = Aradex.messageImage

//////////////////////////////////
//           Indicator          //
//////////////////////////////////
Aradex.indicatorColorOn = "double_punkt_gruen.png"
Aradex.indicatorColorOff = "double_punkt_rot.png"
Aradex.indicatorColor0 = "punkt_rot.png"
Aradex.indicatorColor1 = "punkt_gruen.png"
Aradex.indicatorColor2 = "punkt_normal.png"
Aradex.indicatorColor3 = "punkt_aktiv.png"

//////////////////////////////////
//        NUMERIC INPUT         //
//////////////////////////////////
// Additional Images for non symmetrical Styles
Aradex.numericinputBackgroundImageNormal = "background_dunkel_grau.png"
Aradex.numericinputTextfieldImageEnabled = Aradex.textfieldImageEnabled
Aradex.numericinputTextfieldImageFocus = Aradex.textfieldImageFocus
Aradex.numericinputTextfieldImageDisabled = Aradex.textfieldImageDisabled
Aradex.numericinputTextfieldImageReadOnly = Aradex.textfieldImageReadOnly
Aradex.numericinputButtonLeftImageNormal = "background_dunkel_blau_closed_right.png"
Aradex.numericinputButtonLeftImagePressed = "background_dunkel_blau_closed_right.png"
Aradex.numericinputButtonLeftImageDisabled = "background_dunkel_blau_closed_right.png"
Aradex.numericinputButtonRightImageNormal = "background_dunkel_blau_closed_left.png"
Aradex.numericinputButtonRightImagePressed = "background_dunkel_blau_closed_left.png"
Aradex.numericinputButtonRightImageDisabled = "background_dunkel_blau_closed_left.png"
Aradex.numericinputButtonPlus = "plus.png"
Aradex.numericinputButtonMinus = "minus.png"
Aradex.numericinputButtonPlusMinusHeight = 16
Aradex.numericinputButtonPlusMinusWidth = 16
Aradex.numericinputButtonPlusMinusHorizontalAlignment = Qt.AlignHCenter
Aradex.numericinputButtonPlusMinusVerticalAlignment = Qt.AlignVCenter

// font
Aradex.numericinputFontSizeMiddle = Aradex.fontSize;
Aradex.numericinputFontColorMiddle = Aradex.mediumFontColor
Aradex.numericinputFontSizeLimits = Aradex.fontSize / 2
Aradex.numericinputFontColorLimits = Aradex.lightColor

//////////////////////////////////
//        Browser               //
//////////////////////////////////
Aradex.browserNextImage = "icons/next.svg"
Aradex.browserBackImage = "icons/previous.svg"
Aradex.browserReloadImage = "icons/view-refresh.png"
Aradex.browserUrlInput = "background_textfield_normal.png"
Aradex.browserUrlInputSelected = "background_textfield_selected.png"
Aradex.browserHeaderBackground = "background_grey_top.png"
Aradex.browserZoomInImage = "icons/ZoomIn.svg"
Aradex.browserZoomOutImage = "icons/ZoomOut.svg"
Aradex.browserBorderSize = 7;
// Image Borders
Aradex.browserBorderLeftSize = Aradex.browserBorderSize;
Aradex.browserBorderRightSize = Aradex.browserBorderSize;
Aradex.browserBorderTopSize = Aradex.browserBorderSize;
Aradex.browserBorderBottomSize = Aradex.browserBorderSize;

////////////////////////////////
//      FileChooser           //
////////////////////////////////
Aradex.fileChooserSubcomponentBorderWidth = 4
Aradex.fileChoosermainContainerColor = Aradex.backgroundColor
Aradex.fileChooserfontColor = Aradex.mediumFontColor
Aradex.fileChooserfontSelectedColor = Aradex.lightFontColor
Aradex.fileChooserhighlightColor = Aradex.lightColor
Aradex.fileChooserBackImage  = "icons/moveUp.svg"
Aradex.fileChooserFolderImage  = "icons/folder.svg"
Aradex.fileChooserFileImage  = "icons/file.svg"
Aradex.fileChooserErrorImage = Aradex.errorImage

////////////////////////////////
//      Progress Bar          //
////////////////////////////////
Aradex.progressBarBackground = "progressbar_background.svg"
Aradex.progressBarFilling = "progressbar_filling.svg"
Aradex.progressBarBackgroundBorder = 5
Aradex.progressBarBackgroundBorderLeft = Aradex.progressBarBackgroundBorder
Aradex.progressBarBackgroundBorderRight = Aradex.progressBarBackgroundBorder
Aradex.progressBarBackgroundBorderTop = Aradex.progressBarBackgroundBorder
Aradex.progressBarBackgroundBorderBottom = Aradex.progressBarBackgroundBorder
Aradex.progressBarFillingX = 2
Aradex.progressBarFillingY = 2
Aradex.progressBarFillingSourceWidth = 32
Aradex.progressBarFillingSourceHeight = 32
Aradex.progressBarTextBold = true
Aradex.progressBarTextColorChoice = 0
Aradex.progressBarTextOutline= true
Aradex.progressBarTextOutlineColor = Aradex.mediumFontColor
Aradex.progressBarFillingInBackground = false

//////////////////////////////////
//             	 LOGVIEW        //
//////////////////////////////////
Aradex.logFontColor0 = Aradex.lightFontColor
Aradex.logFontColor1 = Aradex.mediumFontColor
Aradex.logFontColor2 = Aradex.lightColor
Aradex.logFontColor3 = Aradex.mediumColor
Aradex.logFontSize = Aradex.fontSize
Aradex.logFontFamily = Aradex.fontFamily

