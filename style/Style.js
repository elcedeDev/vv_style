// $Revision: 17658 $
.pragma library
//// current filepath
//// This is available in all editors.

Qt.include("ELCEDE_FLAT/ELCEDE_FLAT.js")
Qt.include("Aradex/Aradex.js")
Qt.include("Desktop/Desktop.js")
Qt.include("Globals.js")

var stylePath = "./style";

// default style when opened
var styleName= "ELCEDE_FLAT";


var Style = new Object();
Style["ELCEDE_FLAT"] = ELCEDE_FLAT;
Style["Aradex"] = Aradex;
Style["Desktop"] = Desktop;



//////////////////////////////////////////////
//    Global Parameters for all Styles      //
//////////////////////////////////////////////

var isFast = true;
var focusOn = true;


//////////////////////////////////////////////
//              Initial Sizes               //
//////////////////////////////////////////////

    //Buttons
Style.buttonHeightInitial = 56;
Style.buttonWidthInitial = 136;
Style.buttonHeightInitialSmall = 36;
Style.buttonWidthInitialSmall = 86;
Style.togglebuttonHeightInitial = 56;
Style.togglebuttonWidthInitial = 136;
Style.radiobuttonHeightInitial = 56;
Style.radiobuttonWidthInitial = 136;
Style.radiogroupHeightInitial = 100;
Style.radiogroupWidthInitial = 250;
Style.languageButtonWidthInitial = 136;
Style.languageButtonHeightInitial = 56;
Style.styleButtonWidthInitial = 136;
Style.styleButtonHeightInitial = 56;
    // Text Components
Style.labelHeightInitial = 36;
Style.labelWidthInitial = 106;
Style.textfieldHeightInitial = 36;
Style.textfieldWidthInitial = 116;
Style.multilinetextHeightInitial = 50;
Style.multilinetextWidthInitial = 140;
Style.NumericInputHeightInitial = 36
Style.NumericInputWidthInitial = 172
    // Meters
Style.meter360HeightInitial = 300
Style.meter360WidthInitial = 300
Style.linearHeightInitial = 300
Style.linearWidthInitial = 150
Style.thermometerHeightInitial = 360
Style.thermometerWidthInitial = 200
Style.progressbarHeightInitial = 27
Style.progressbarWidthInitial = 372

// Widgets
Style.PictureHeightInitial = 100
Style.PictureWidgetWidthInitial = 100
Style.IndicatorSingleHeightInitial = 30
Style.IndicatorSingleWidthInitial = 30
Style.IndicatorDoubleHeightInitial = 30
Style.IndicatorDoubleWidthInitial = 60
Style.ProgressBarHeightInitial = 30
Style.ProgressBarWidthInitial = 180



// Container
Style.containerWidthInitial = 300
Style.containerHeightInitial = 200
Style.segmentWidthInitial = 300
Style.segmentHeightInitial = 200
Style.labellistWidthInitial = 300
Style.labellistHeightInitial = 150
Style.themeshapeWidthInitial = 300
Style.themeshapeHeightInitial = 200

// Dialogs
Style.dialogButtonHeightInitial = 36;
Style.dialogButtonWidthInitial = 86;
Style.dialogMsgBoxHeightInitial = 240;
Style.dialogMsgBoxWidthInitial = 360;
Style.dialogYesNoHeightInitial = 240;
Style.dialogYesNoWidthInitial = 360;
Style.dialogHeightInitial = 240;
Style.dialogWidthInitial = 360;
Style.dialogFileChooserHeightInitial = 540;
Style.dialogFileChooserWidthInitial = 760


// Complex Widgets
Style.ErrorWidgetHeightInitial = 40
Style.ErrorWidgetWidthInitial = 480
Style.FilechooserHeightInitial = 400
Style.FilechooserWidthInitial = 250


/**
* Given a source directory and a target filename, return the relative
* file path from source to target.
* @param source {String} directory path to start from for traversal
* @param target {String} directory path and filename to seek from source
* @return Relative path (e.g. "../../style.css") as {String}
*/
function getRelativePath(source, target) {
    var sep = (source.indexOf("/") !== -1) ? "/" : "\\",
    targetArr = target.split(sep),
    sourceArr = source.split(sep),
    filename = targetArr.pop(),
    targetPath = targetArr.join(sep),
    relativePath = "";
    while (targetPath.indexOf(sourceArr.join(sep)) === -1) {
        sourceArr.pop();
        relativePath += ".." + sep;
    }
    var relPathArr = targetArr.slice(sourceArr.length);
    relPathArr.length && (relativePath += relPathArr.join(sep) + sep);
    return relativePath + filename;
}


