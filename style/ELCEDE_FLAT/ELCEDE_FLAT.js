// $Revision: 17490 $
.pragma library

var ELCEDE_FLAT = new Object();

//////////////////////////////////
//            GENERAL           //
//////////////////////////////////
ELCEDE_FLAT.imageFolder =  Qt.resolvedUrl("images/")
ELCEDE_FLAT.imagePath =  Qt.resolvedUrl("images/")

//ELCEDE_FLAT.fontFamily = "ELCEDE DIN"
ELCEDE_FLAT.fontFamily = "Roboto Regular"
//ELCEDE_FLAT.fontFamilyBold = "ELCEDE DIN-Black"
ELCEDE_FLAT.fontFamilyBold = "Roboto Bold"
//ELCEDE_FLAT.fontSize = 14
ELCEDE_FLAT.fontSize = 13
ELCEDE_FLAT.backgroundColor = "#d2d2d2"//"#999999"
ELCEDE_FLAT.lightColor = "#7F7F7F"
ELCEDE_FLAT.mediumColor = "#0F0F0F"
ELCEDE_FLAT.lightFontColor = "#FFFFFF"
ELCEDE_FLAT.subLightFontColor = "#FFFFFF"
ELCEDE_FLAT.mediumFontColor = "#000000"//"#3E3E3E"
ELCEDE_FLAT.subMediumFontColor = "#000000" //"#4C4C4C"
ELCEDE_FLAT.errorImage = "icons/Error.svg"
ELCEDE_FLAT.warningImage = "icons/Warning.svg"
ELCEDE_FLAT.messageImage = "icons/Message.svg"

//////////////////////////////////
//            FOCUS             //
//////////////////////////////////
ELCEDE_FLAT.focusBorderLeft = 7
ELCEDE_FLAT.focusBorderRight = 7
ELCEDE_FLAT.focusBorderTop = 7
ELCEDE_FLAT.focusBorderBottom = 7
ELCEDE_FLAT.focusShiftLeft = -1
ELCEDE_FLAT.focusShiftRight = -1
ELCEDE_FLAT.focusShiftTop = -1
ELCEDE_FLAT.focusShiftBottom = -1
ELCEDE_FLAT.focusOpacity = 1
ELCEDE_FLAT.focusImage = ""

//////////////////////////////////
//            THEME             //
//////////////////////////////////
ELCEDE_FLAT.themeImage0 = "" //background_hell_grau.png"
ELCEDE_FLAT.themeImage1 = "" //"background_hell_grau.png"//"background_hell_blau.png"
ELCEDE_FLAT.themeImage2 = "" //"background_dunkel_grau.png"
ELCEDE_FLAT.themeImage3 = "" //"background_dunkel_grau.png" //"background_dunkel_blau.png"
ELCEDE_FLAT.themeImage4 = "background_rot.png"
ELCEDE_FLAT.themeImage5 = ""
ELCEDE_FLAT.themeImage6 = ""
ELCEDE_FLAT.themeImage7 = ""
ELCEDE_FLAT.themeColor0 = "#666666" //"#EBEBEB"
ELCEDE_FLAT.themeColor1 = "#333333"
ELCEDE_FLAT.themeColor2 = "#00FF00" //"#3E3E3E"
ELCEDE_FLAT.themeColor3 = "#FF0000" //"#868686"
ELCEDE_FLAT.themeColor4 = "#0E0E0E"
ELCEDE_FLAT.themeColor5 = ""
ELCEDE_FLAT.themeColor6 = ""
ELCEDE_FLAT.themeColor7 = ""
ELCEDE_FLAT.themeHasImage = true
ELCEDE_FLAT.themeBorder = 7
ELCEDE_FLAT.themeBorderLeft = ELCEDE_FLAT.themeBorder
ELCEDE_FLAT.themeBorderRight = ELCEDE_FLAT.themeBorder
ELCEDE_FLAT.themeBorderTop = ELCEDE_FLAT.themeBorder
ELCEDE_FLAT.themeBorderBottom = ELCEDE_FLAT.themeBorder

//////////////////////////////////
//       VIRTUAL KEYBOARD       //
//////////////////////////////////
ELCEDE_FLAT.virtualKeyboardBackgroundColor = "black"
ELCEDE_FLAT.virtualKeyboardKeyBackgroundColor = "dimgrey"
ELCEDE_FLAT.virtualKeyboardKeyTextColor = "white"
ELCEDE_FLAT.virtualKeyboardControlKeyBackgroundColor = "#353535" // a dark grey
ELCEDE_FLAT.virtualKeyboardControlKeyTextColor = "white"
ELCEDE_FLAT.virtualKeyboardControlKeyTextActiveColor = "black"

//////////////////////////////////
//          CONTAINER           //
//////////////////////////////////
// Colors correspond to Theme Colors above
// [main themeshape colorchoice, header themeshape colorchoice, fontcolor]
ELCEDE_FLAT.containerColors0 = [0,0,0]//[1,3,2]
ELCEDE_FLAT.containerColors1 = [1,1,1]//[2,1,1]
ELCEDE_FLAT.containerColors2 = [2,2,2]//[0,1,3]
ELCEDE_FLAT.containerColors3 = [3,3,3]//[4,0,2]
ELCEDE_FLAT.containerHeaderHeight = 32
ELCEDE_FLAT.containerHeaderWidth = 120
ELCEDE_FLAT.containerHeaderVisible = true
ELCEDE_FLAT.containerHeaderTextInBorder = false
ELCEDE_FLAT.containerFitText = true
ELCEDE_FLAT.containerTextXShift = ELCEDE_FLAT.fontSize
ELCEDE_FLAT.containerTextYShift = ELCEDE_FLAT.fontSize


//////////////////////////////////
//           Segment            //
//////////////////////////////////
ELCEDE_FLAT.segmentImage0 = "trenner_hell_grau.png"
ELCEDE_FLAT.segmentImage1 = "trenner_hell_grau.png"//"trenner_hell_blau.png"
ELCEDE_FLAT.segmentImage2 = "trenner_dunkel_grau.png"
ELCEDE_FLAT.segmentImage3 = "trenner_dunkel_grau.png" //"trenner_dunkel_blau.png"
ELCEDE_FLAT.segmentImage4 = "trenner_rot.png"
ELCEDE_FLAT.segmentImage5 = ""
ELCEDE_FLAT.segmentImage6 = ""
ELCEDE_FLAT.segmentImage7 = ""
ELCEDE_FLAT.segmentImageHeight = 40
ELCEDE_FLAT.segmentBorderLeft = 40
ELCEDE_FLAT.segmentBorderRight = 6
ELCEDE_FLAT.segmentBorderTop = 32
ELCEDE_FLAT.segmentBorderBottom = 0

ELCEDE_FLAT.segmentHeight = 200
ELCEDE_FLAT.segmentWidth = 120

//////////////////////////////////
//          LABELLIST           //
//////////////////////////////////
ELCEDE_FLAT.labellistColor0 = ELCEDE_FLAT.themeColor0
ELCEDE_FLAT.labellistColor1 = ELCEDE_FLAT.themeColor1
ELCEDE_FLAT.labellistColor2 = ELCEDE_FLAT.themeColor2
ELCEDE_FLAT.labellistColor3 = ELCEDE_FLAT.themeColor3
ELCEDE_FLAT.labellistColor4 = ELCEDE_FLAT.themeColor4
ELCEDE_FLAT.labellistLeftRightMargin = 4
ELCEDE_FLAT.labellistSeperatorWidth = 1



//////////////////////////////////
//            BUTTON            //
//////////////////////////////////
ELCEDE_FLAT.buttonImageNormal = "ButtonNormal.png"//"background_btn_normal_normal.png" //"ButtonNormal.svg"
ELCEDE_FLAT.buttonImagePressed = "ButtonPressed.png"//"background_btn_normal_aktive.png" //"ButtonPressed.svg"
ELCEDE_FLAT.buttonImageDisabled = "ButtonNormal.png"//"background_btn_normal_normal.png"
ELCEDE_FLAT.buttonImageFocus = ""//"background_btn_normal_focus.png" //"ButtonPressed.svg" 
ELCEDE_FLAT.buttonBorderSize = 7;
ELCEDE_FLAT.buttonTextBorderSize = 3
ELCEDE_FLAT.buttonFontSize = ELCEDE_FLAT.fontSize
ELCEDE_FLAT.buttonTextMargin = 3;
ELCEDE_FLAT.buttonFontColor = ELCEDE_FLAT.lightFontColor
ELCEDE_FLAT.buttonFontPressedColor = ELCEDE_FLAT.mediumFontColor
ELCEDE_FLAT.buttonFontDisabledColor =  ELCEDE_FLAT.lightColor //ELCEDE_FLAT.subLightFontColor
ELCEDE_FLAT.buttonFontFamily = ELCEDE_FLAT.fontFamily
ELCEDE_FLAT.buttonEmpty = "Empty.png"
// Image Borders
ELCEDE_FLAT.buttonBorderLeftSize = ELCEDE_FLAT.buttonBorderSize;
ELCEDE_FLAT.buttonBorderRightSize = ELCEDE_FLAT.buttonBorderSize;
ELCEDE_FLAT.buttonBorderTopSize = ELCEDE_FLAT.buttonBorderSize;
ELCEDE_FLAT.buttonBorderBottomSize = ELCEDE_FLAT.buttonBorderSize;

// Text Borders
ELCEDE_FLAT.buttonTextBorderLeftSize = ELCEDE_FLAT.buttonTextBorderSize;
ELCEDE_FLAT.buttonTextBorderRightSize = ELCEDE_FLAT.buttonTextBorderSize;
ELCEDE_FLAT.buttonTextBorderTopSize = ELCEDE_FLAT.buttonTextBorderSize;
ELCEDE_FLAT.buttonTextBorderBottomSize = ELCEDE_FLAT.buttonTextBorderSize;

//margins
ELCEDE_FLAT.buttonTextLeftMargin = 0;
ELCEDE_FLAT.buttonTextRightMargin = 0;
ELCEDE_FLAT.buttonTextTopMargin = 0;
ELCEDE_FLAT.buttonTextBottomMargin = 0;
// text Alignement
ELCEDE_FLAT.buttonHorizontalAlignement = Qt.AlignHCenter
ELCEDE_FLAT.buttonVerticalAlignement = Qt.AlignVCenter
// Button Shift when Pressed
ELCEDE_FLAT.buttonPressedLeftShift = -1
ELCEDE_FLAT.buttonPressedRightShift = -1
ELCEDE_FLAT.buttonPressedTopShift = 0
ELCEDE_FLAT.buttonPressedBottomShift = -1
// Animations
ELCEDE_FLAT.buttonSizeAnimationTime = 0
ELCEDE_FLAT.buttonTextAnimationTime = 0
// Focus
ELCEDE_FLAT.buttonFocusBorderLeft = ELCEDE_FLAT.focusBorderLeft
ELCEDE_FLAT.buttonFocusBorderRight = ELCEDE_FLAT.focusBorderRight
ELCEDE_FLAT.buttonFocusBorderTop = ELCEDE_FLAT.focusBorderTop
ELCEDE_FLAT.buttonFocusBorderBottom = ELCEDE_FLAT.focusBorderBottom
ELCEDE_FLAT.buttonFocusShiftLeft = ELCEDE_FLAT.focusShiftLeft
ELCEDE_FLAT.buttonFocusShiftRight = ELCEDE_FLAT.focusShiftRight
ELCEDE_FLAT.buttonFocusShiftTop = ELCEDE_FLAT.focusShiftTop
ELCEDE_FLAT.buttonFocusShiftBottom = ELCEDE_FLAT.focusShiftBottom
// forground button
ELCEDE_FLAT.buttonForegroundImageHeight = 32
ELCEDE_FLAT.buttonForegroundImageWidth = 32

//////////////////////////////////
//       LANGUAGE BUTTON        //
//////////////////////////////////

ELCEDE_FLAT.languageButtonImage = new Object()
ELCEDE_FLAT.languageButtonImage["en"] = "lang/United-Kingdom-64_1.png"
ELCEDE_FLAT.languageButtonImage["de"] = "lang/Germany-64_1.png"
ELCEDE_FLAT.languageButtonImage["fr"] = "lang/France-64_1.png"
ELCEDE_FLAT.languageButtonImage["es"] = "lang/Spain-64_1.png"
ELCEDE_FLAT.languageButtonImage["ch"] = "lang/China-64.png"
ELCEDE_FLAT.languageButtonImage["hu"] = "lang/Hungary-64_1.png"
ELCEDE_FLAT.languageButtonImage["jp"] = "lang/Japan-64_1.png"
ELCEDE_FLAT.languageButtonImage["dk"] = "lang/Denmark-64_1.png"
ELCEDE_FLAT.languageButtonImage["cz"] = "lang/Czech-64_1.png"
ELCEDE_FLAT.languagebuttonImageHeight = 48
ELCEDE_FLAT.languagebuttonImageWidth = 48
ELCEDE_FLAT.languagebuttonImageHorizontalAlignement = Qt.AlignRight
ELCEDE_FLAT.languagebuttonImageVerticalAlignement = Qt.AlignVCenter
ELCEDE_FLAT.languagebuttonImageXShift = - 16
ELCEDE_FLAT.languagebuttonImageYShift = 0

//////////////////////////////////
//         logFile BUTTON        //
//////////////////////////////////
ELCEDE_FLAT.logFileButtonImage= "icons/logFile.svg"
ELCEDE_FLAT.logFileButtonImageHeight = 48
ELCEDE_FLAT.logFileButtonImageWidth = 48
ELCEDE_FLAT.logFileButtonImageHorizontalAlignement = Qt.AlignHCenter
ELCEDE_FLAT.logFileButtonImageVerticalAlignement = Qt.AlignVCenter
ELCEDE_FLAT.logFileButtonImageXShift = 0
ELCEDE_FLAT.logFileButtonImageYShift = 0

//////////////////////////////////
//         STYLE  BUTTON        //
//////////////////////////////////
ELCEDE_FLAT.styleButtonImage = new Object()
ELCEDE_FLAT.styleButtonImage["ELCEDE"] = ""
ELCEDE_FLAT.styleButtonImage["WinXP"] = ""
ELCEDE_FLAT.stylebuttonImageHeight = 0
ELCEDE_FLAT.stylebuttonImageWidth = 0
ELCEDE_FLAT.stylebuttonImageHorizontalAlignement = Qt.AlignRight
ELCEDE_FLAT.stylebuttonImageVerticalAlignement = Qt.AlignVCenter
ELCEDE_FLAT.stylebuttonImageXShift = 0
ELCEDE_FLAT.stylebuttonImageYShift = 0

//////////////////////////////////
//            TOGGLEBUTTON      //
//////////////////////////////////
ELCEDE_FLAT.togglebuttonImageNormal = "ButtonNormal.png" //ToggleNormalOff.png"//"background_btn_normal_normal.png"
ELCEDE_FLAT.togglebuttonImagePressed = "ButtonPressed.png" //"ToggleNormalOn.png"//"background_btn_normal_normal.png"
ELCEDE_FLAT.togglebuttonImageDisabled = "ButtonNormal.png" //"ToggleNormalOn.png" //"background_btn_normal_normal.png"
ELCEDE_FLAT.togglebuttonImagePressedAndDisabled = "ToggleNormalOn.png" //"background_btn_normal_normal.png"
ELCEDE_FLAT.togglebuttonImageFocus = ELCEDE_FLAT.focusImage
ELCEDE_FLAT.togglebuttonLightImageOff = "punkt_normal.png"
ELCEDE_FLAT.togglebuttonLightImageOn = "punkt_gruen.png"
ELCEDE_FLAT.togglebuttonLightHorizontalAlignement = Qt.AlignRight
ELCEDE_FLAT.togglebuttonLightVerticalAlignement = Qt.AlignTop
ELCEDE_FLAT.togglebuttonLightImageHeight = 30
ELCEDE_FLAT.togglebuttonLightImageWidth = 30

ELCEDE_FLAT.togglebuttonBorderSize = 0;
ELCEDE_FLAT.togglebuttonTextBorderSize = 3
ELCEDE_FLAT.togglebuttonFontSize = ELCEDE_FLAT.fontSize
ELCEDE_FLAT.togglebuttonTextMargin = 3;

ELCEDE_FLAT.togglebuttonFontColor = ELCEDE_FLAT.lightFontColor
ELCEDE_FLAT.togglebuttonFontPressedColor = ELCEDE_FLAT.mediumFontColor
ELCEDE_FLAT.togglebuttonFontDisabledColor = ELCEDE_FLAT.lightColor
ELCEDE_FLAT.togglebuttonFontPressedAndDisabledColor = ELCEDE_FLAT.lightColor
ELCEDE_FLAT.togglebuttonFocusBorderColor = "#00000000"
ELCEDE_FLAT.togglebuttonFontFamily = ELCEDE_FLAT.fontFamily
// Image Borders
ELCEDE_FLAT.togglebuttonBorderLeftSize = ELCEDE_FLAT.togglebuttonBorderSize;
ELCEDE_FLAT.togglebuttonBorderRightSize = ELCEDE_FLAT.togglebuttonBorderSize;
ELCEDE_FLAT.togglebuttonBorderTopSize =  ELCEDE_FLAT.togglebuttonBorderSize;
ELCEDE_FLAT.togglebuttonBorderBottomSize = ELCEDE_FLAT.togglebuttonBorderSize;

// Text Borders
ELCEDE_FLAT.togglebuttonTextBorderLeftSize = ELCEDE_FLAT.togglebuttonTextBorderSize;
ELCEDE_FLAT.togglebuttonTextBorderRightSize = ELCEDE_FLAT.togglebuttonTextBorderSize;
ELCEDE_FLAT.togglebuttonTextBorderTopSize = ELCEDE_FLAT.togglebuttonTextBorderSize;
ELCEDE_FLAT.togglebuttonTextBorderBottomSize = ELCEDE_FLAT.togglebuttonTextBorderSize;

//margins
ELCEDE_FLAT.togglebuttonTextLeftMargin = 5;
ELCEDE_FLAT.togglebuttonTextRightMargin = 0;
ELCEDE_FLAT.togglebuttonTextTopMargin = 5;
ELCEDE_FLAT.togglebuttonTextBottomMargin = 0;
// text Alignement
ELCEDE_FLAT.togglebuttonHorizontalAlignement = Qt.AlignLeft
ELCEDE_FLAT.togglebuttonVerticalAlignement = Qt.AlignBottom

// Button Shift when Pressed
ELCEDE_FLAT.togglebuttonPressedLeftShift = -2
ELCEDE_FLAT.togglebuttonPressedRightShift = -2
ELCEDE_FLAT.togglebuttonPressedTopShift = -2
ELCEDE_FLAT.togglebuttonPressedBottomShift = -2
// Animations
ELCEDE_FLAT.togglebuttonSizeAnimationTime = 0
ELCEDE_FLAT.togglebuttonTextAnimationTime = 0
// Focus
ELCEDE_FLAT.togglebuttonFocusBorderLeft = ELCEDE_FLAT.focusBorderLeft
ELCEDE_FLAT.togglebuttonFocusBorderRight = ELCEDE_FLAT.focusBorderRight
ELCEDE_FLAT.togglebuttonFocusBorderTop = ELCEDE_FLAT.focusBorderTop
ELCEDE_FLAT.togglebuttonFocusBorderBottom = ELCEDE_FLAT.focusBorderBottom
ELCEDE_FLAT.togglebuttonFocusShiftLeft = ELCEDE_FLAT.focusShiftLeft
ELCEDE_FLAT.togglebuttonFocusShiftRight = ELCEDE_FLAT.focusShiftRight
ELCEDE_FLAT.togglebuttonFocusShiftTop = ELCEDE_FLAT.focusShiftTop
ELCEDE_FLAT.togglebuttonFocusShiftBottom = ELCEDE_FLAT.focusShiftBottom
// forground button
ELCEDE_FLAT.togglebuttonForegroundImageHeight = 32
ELCEDE_FLAT.togglebuttonForegroundImageWidth = 32

//////////////////////////////////////
//            TOGGLEBUTTONREVERSE   //
//////////////////////////////////////
ELCEDE_FLAT.togglebuttonreverseImageNormal = "ButtonPressed.png"//"background_btn_normal_normal.png"
ELCEDE_FLAT.togglebuttonreverseImagePressed = "ButtonNormal.png"//"background_btn_normal_normal.png"
ELCEDE_FLAT.togglebuttonreverseLightImageOff = "punkt_gruen.png"
ELCEDE_FLAT.togglebuttonreverseLightImageOn = "punkt_normal.png"

//////////////////////////////////
//          RADIOBUTTON         //
//////////////////////////////////
ELCEDE_FLAT.radiobuttonImageNormal = "ButtonNormal.png" //"background_btn_normal_normal.png"
ELCEDE_FLAT.radiobuttonImagePressed = "ButtonPressed.png" //"background_btn_normal_aktive_open_right.png"
ELCEDE_FLAT.radiobuttonImageDisabled = "ButtonNormal.png" //"background_btn_normal_normal.png"
ELCEDE_FLAT.radiobuttonImagePressedAndDisabled = "ButtonNormal.png"// "background_btn_normal_normal.png"
ELCEDE_FLAT.radiobuttonImageFocus = ELCEDE_FLAT.focusImage
ELCEDE_FLAT.radiobuttonLightImageOff = "" //"punkt_normal.png"
ELCEDE_FLAT.radiobuttonLightImageOn = ""//"punkt_aktiv.png"
ELCEDE_FLAT.radiobuttonLightImageHeight = 30
ELCEDE_FLAT.radiobuttonLightImageWidth = 30
ELCEDE_FLAT.radiobuttonLightHorizontalAlignement = Qt.AlignRight
ELCEDE_FLAT.radiobuttonLightVerticalAlignement = Qt.AlignTop
ELCEDE_FLAT.radiobuttonBorderSize = 0;
ELCEDE_FLAT.radiobuttonTextBorderSize = 3
ELCEDE_FLAT.radiobuttonFontSize = ELCEDE_FLAT.fontSize
ELCEDE_FLAT.radiobuttonTextMargin = 3;
ELCEDE_FLAT.radiobuttonFontColor = ELCEDE_FLAT.lightFontColor
ELCEDE_FLAT.radiobuttonFontPressedColor = ELCEDE_FLAT.mediumFontColor
ELCEDE_FLAT.radiobuttonFontDisabledColor = ELCEDE_FLAT.lightColor
ELCEDE_FLAT.radiobuttonFontPressedAndDisabledColor = ELCEDE_FLAT.lightColor
ELCEDE_FLAT.radiobuttonFocusBorderColor = "#00000000"
ELCEDE_FLAT.radiobuttonFontFamily = ELCEDE_FLAT.fontFamily
// Image Borders
ELCEDE_FLAT.radiobuttonBorderLeftSize = ELCEDE_FLAT.radiobuttonBorderSize;
ELCEDE_FLAT.radiobuttonBorderRightSize = ELCEDE_FLAT.radiobuttonBorderSize;
ELCEDE_FLAT.radiobuttonBorderTopSize = ELCEDE_FLAT.radiobuttonBorderSize;
ELCEDE_FLAT.radiobuttonBorderBottomSize = ELCEDE_FLAT.radiobuttonBorderSize;
// Text Borders
ELCEDE_FLAT.radiobuttonTextBorderLeftSize = ELCEDE_FLAT.radiobuttonTextBorderSize;
ELCEDE_FLAT.radiobuttonTextBorderRightSize = ELCEDE_FLAT.radiobuttonTextBorderSize;
ELCEDE_FLAT.radiobuttonTextBorderTopSize = ELCEDE_FLAT.radiobuttonTextBorderSize;
ELCEDE_FLAT.radiobuttonTextBorderBottomSize = ELCEDE_FLAT.radiobuttonTextBorderSize;

//margins
ELCEDE_FLAT.radiobuttonTextLeftMargin = 5;
ELCEDE_FLAT.radiobuttonTextRightMargin = 0;
ELCEDE_FLAT.radiobuttonTextTopMargin = 0;
ELCEDE_FLAT.radiobuttonTextBottomMargin = 0;
// text Alignement
ELCEDE_FLAT.radiobuttonHorizontalAlignement = Qt.AlignLeft
ELCEDE_FLAT.radiobuttonVerticalAlignement = Qt.AlignTop // Qt.AlignBottom

// Button Shift when Pressed
ELCEDE_FLAT.radiobuttonPressedLeftShift = -1
ELCEDE_FLAT.radiobuttonPressedRightShift = -1
ELCEDE_FLAT.radiobuttonPressedTopShift = -1
ELCEDE_FLAT.radiobuttonPressedBottomShift = -1
// Animations
ELCEDE_FLAT.radiobuttonSizeAnimationTime = 0
ELCEDE_FLAT.radiobuttonTextAnimationTime = 0
// Focus
ELCEDE_FLAT.radiobuttonFocusBorderLeft = ELCEDE_FLAT.focusBorderLeft
ELCEDE_FLAT.radiobuttonFocusBorderRight = ELCEDE_FLAT.focusBorderRight
ELCEDE_FLAT.radiobuttonFocusBorderTop = ELCEDE_FLAT.focusBorderTop
ELCEDE_FLAT.radiobuttonFocusBorderBottom = ELCEDE_FLAT.focusBorderBottom
ELCEDE_FLAT.radiobuttonFocusShiftLeft = ELCEDE_FLAT.focusShiftLeft
ELCEDE_FLAT.radiobuttonFocusShiftRight = ELCEDE_FLAT.focusShiftRight
ELCEDE_FLAT.radiobuttonFocusShiftTop = ELCEDE_FLAT.focusShiftTop
ELCEDE_FLAT.radiobuttonFocusShiftBottom = ELCEDE_FLAT.focusShiftBottom

//////////////////////////////////
//             	   LABEL        //
//////////////////////////////////
ELCEDE_FLAT.labelImage0 = "" //"background_dunkel_grau.png"
ELCEDE_FLAT.labelImage1 = "" //"background_hell_grau.png"
ELCEDE_FLAT.labelImage2 = "" //"background_hell_blau.png"
ELCEDE_FLAT.labelImage3 = ""//"background_hell_grau.png"//"background_dunkel_blau.png"
ELCEDE_FLAT.labelFontColor0 = ELCEDE_FLAT.lightFontColor
ELCEDE_FLAT.labelFontColor1 = ELCEDE_FLAT.mediumFontColor
ELCEDE_FLAT.labelFontColor2 = ELCEDE_FLAT.lightColor
ELCEDE_FLAT.labelFontColor3 = ELCEDE_FLAT.mediumColor
ELCEDE_FLAT.labelBorderSize = 7;
ELCEDE_FLAT.labelTextBorderSize = 2
ELCEDE_FLAT.labelFontSize = ELCEDE_FLAT.fontSize
ELCEDE_FLAT.labelTextMargin = 3;
ELCEDE_FLAT.labelFontColor = "black"
ELCEDE_FLAT.labelFocusBorderColor = "#00000000"
// text Alignement
ELCEDE_FLAT.labelHorizontalAlignement = Qt.AlignLeft
ELCEDE_FLAT.labelVerticalAlignement = Qt.AlignBottom
ELCEDE_FLAT.labelFontFamily = ELCEDE_FLAT.fontFamily
// image borders
ELCEDE_FLAT.labelTextBorderLeftSize = ELCEDE_FLAT.labelTextBorderSize
ELCEDE_FLAT.labelTextBorderRightSize = ELCEDE_FLAT.labelTextBorderSize
ELCEDE_FLAT.labelTextBorderTopSize = ELCEDE_FLAT.labelTextBorderSize
ELCEDE_FLAT.labelTextBorderBottomSize = ELCEDE_FLAT.labelTextBorderSize
// text Borders
ELCEDE_FLAT.labelBorderLeftSize = ELCEDE_FLAT.labelBorderSize
ELCEDE_FLAT.labelBorderRightSize = ELCEDE_FLAT.labelBorderSize
ELCEDE_FLAT.labelBorderTopSize = ELCEDE_FLAT.labelBorderSize
ELCEDE_FLAT.labelBorderBottomSize = ELCEDE_FLAT.labelBorderSize
// margins
ELCEDE_FLAT.labelTextLeftMargin = ELCEDE_FLAT.labelTextMargin
ELCEDE_FLAT.labelTextRightMargin = 0//ELCEDE_FLAT.labelTextMargin
ELCEDE_FLAT.labelTextTopMargin = 0//ELCEDE_FLAT.labelTextMargin
ELCEDE_FLAT.labelTextBottomMargin = ELCEDE_FLAT.labelTextMargin
// animation
ELCEDE_FLAT.labelAnimationHighlightColor = ELCEDE_FLAT.lightColor
ELCEDE_FLAT.labelAnimationHighlightHoldTime = 1000
ELCEDE_FLAT.labelAnimationHighlightTransitionTime = 2500


//////////////////////////////////
//          TextField           //
//////////////////////////////////
ELCEDE_FLAT.textfieldImageEnabled = "input.png" //"input_normal.png" //"textfield_background_normal.svg"
ELCEDE_FLAT.textfieldImageFocus = "input.png" //"input_normal.png" //"textfield_background_selected.svg"
ELCEDE_FLAT.textfieldBorderSize = 7
ELCEDE_FLAT.textfieldFocusBorderSize = 7
ELCEDE_FLAT.textfieldTextBorderSize = 7
ELCEDE_FLAT.textfieldFontSize = ELCEDE_FLAT.fontSize;

ELCEDE_FLAT.fontColor1 = ELCEDE_FLAT.lightFontColor
ELCEDE_FLAT.fontColor0 = ELCEDE_FLAT.mediumFontColor
ELCEDE_FLAT.fontColor2 = ELCEDE_FLAT.lightColor
ELCEDE_FLAT.fontColor3 = ELCEDE_FLAT.mediumColor

ELCEDE_FLAT.textfieldSelectionColor = ELCEDE_FLAT.mediumFontColor
ELCEDE_FLAT.textfieldSelectedTextColor = ELCEDE_FLAT.lightFontColor
ELCEDE_FLAT.textfieldFontFamily = ELCEDE_FLAT.fontFamily
ELCEDE_FLAT.textfieldHorizontalAlignement = Qt.AlignLeft
// Disabled - read-only
ELCEDE_FLAT.textfieldImageDisabled = "InputDisabled.png" //"input_normal.png" //"textfield_background_normal.svg"
ELCEDE_FLAT.textfieldImageReadOnly = "Label.png" //"input_normal.png" //"textfield_background_normal.svg"
ELCEDE_FLAT.textfieldFontReadOnlyColor = ELCEDE_FLAT.lightFontColor
ELCEDE_FLAT.textfieldFontInactiveColor = ELCEDE_FLAT.subMediumFontColor
ELCEDE_FLAT.textfieldDisabledOpacity = 0.7
ELCEDE_FLAT.textfieldReadOnlyOpacity = 0.7
// Placeholder Text
ELCEDE_FLAT.textfieldPlaceholderTextSize = ELCEDE_FLAT.textfieldFontSize * 0.7
ELCEDE_FLAT.textfieldFontPlaceholderColor = ELCEDE_FLAT.lightFontColor
ELCEDE_FLAT.textfieldPlaceholderOpacity = 0.3

// image Borders
ELCEDE_FLAT.textfieldBorderLeftSize = ELCEDE_FLAT.textfieldBorderSize;
ELCEDE_FLAT.textfieldBorderRightSize = ELCEDE_FLAT.textfieldBorderSize;
ELCEDE_FLAT.textfieldBorderTopSize = ELCEDE_FLAT.textfieldBorderSize;
ELCEDE_FLAT.textfieldBorderBottomSize = ELCEDE_FLAT.textfieldBorderSize;

// image text Borders
ELCEDE_FLAT.textfieldTextBorderLeftSize = 7//ELCEDE_FLAT.textfieldTextBorderSize;
ELCEDE_FLAT.textfieldTextBorderRightSize = 4//ELCEDE_FLAT.textfieldTextBorderSize;
ELCEDE_FLAT.textfieldTextBorderTopSize = 4//ELCEDE_FLAT.textfieldTextBorderSize;
ELCEDE_FLAT.textfieldTextBorderBottomSize = 4//ELCEDE_FLAT.textfieldTextBorderSize;

// Focus
ELCEDE_FLAT.textfieldFocusBorderLeft = ELCEDE_FLAT.focusBorderLeft
ELCEDE_FLAT.textfieldFocusBorderRight = ELCEDE_FLAT.focusBorderRight
ELCEDE_FLAT.textfieldFocusBorderTop = ELCEDE_FLAT.focusBorderTop
ELCEDE_FLAT.textfieldFocusBorderBottom = ELCEDE_FLAT.focusBorderBottom
ELCEDE_FLAT.textfieldFocusShiftLeft = 0
ELCEDE_FLAT.textfieldFocusShiftRight = 0
ELCEDE_FLAT.textfieldFocusShiftTop = 0
ELCEDE_FLAT.textfieldFocusShiftBottom = 0
ELCEDE_FLAT.textfieldFocusShiftBottom = 0
ELCEDE_FLAT.textfieldFocusOpacity = 1


//////////////////////////////////
//      MultiLineTextField      //
//////////////////////////////////
ELCEDE_FLAT.multiImageEnabled = "input.png" //"input_normal.png" //"textfield_background_normal.svg"
ELCEDE_FLAT.multiImageFocus = "input.png" //"input_normal.png" //"textfield_background_selected.svg"
ELCEDE_FLAT.multiBorderSize = 7
ELCEDE_FLAT.multiTextBorderSize = 3
ELCEDE_FLAT.multiFocusBorderSize = 7
ELCEDE_FLAT.multiTextMargin = 3
ELCEDE_FLAT.multiFontSize = ELCEDE_FLAT.fontSize;

ELCEDE_FLAT.multiFontColor0 = ELCEDE_FLAT.lightFontColor
ELCEDE_FLAT.multiFontColor1 = ELCEDE_FLAT.mediumFontColor
ELCEDE_FLAT.multiFontColor2 = ELCEDE_FLAT.lightColor
ELCEDE_FLAT.multiFontColor3 = ELCEDE_FLAT.mediumColor

ELCEDE_FLAT.multiSelectionColor = ELCEDE_FLAT.mediumFontColor
ELCEDE_FLAT.multiSelectedTextColor = ELCEDE_FLAT.lightFontColor
ELCEDE_FLAT.multiFontFamily = ELCEDE_FLAT.fontFamily
ELCEDE_FLAT.multiHorizontalAlignement = Qt.AlignLeft
// Disabled ReadOnly
ELCEDE_FLAT.multiImageDisabled = "Label.png" //"input_normal.png" //"textfield_background_normal.svg"
ELCEDE_FLAT.multiImageReadOnly = "Label.png" //"input_normal.png" //"textfield_background_normal.svg"
ELCEDE_FLAT.multiFontReadOnlyColor =  ELCEDE_FLAT.subMediumFontColor
ELCEDE_FLAT.multiFontInactiveColor =  ELCEDE_FLAT.subMediumFontColor
ELCEDE_FLAT.multiDisabledOpacity = 0.7
ELCEDE_FLAT.multiReadOnlyOpacity = 0.7
// PlaceholderText
ELCEDE_FLAT.multiPlaceholderTextSize = ELCEDE_FLAT.multiFontSize * 0.7;
ELCEDE_FLAT.multiPlaceholderColor0 = ELCEDE_FLAT.lightFontColor
ELCEDE_FLAT.multiPlaceholderColor1 = ELCEDE_FLAT.mediumFontColor
ELCEDE_FLAT.multiPlaceholderColor2 = ELCEDE_FLAT.lightColor
ELCEDE_FLAT.multiPlaceholderColor3 = ELCEDE_FLAT.mediumColor
ELCEDE_FLAT.multiPlaceholderOpacity = 0.3

// image Borders
ELCEDE_FLAT.multiBorderLeftSize = ELCEDE_FLAT.multiBorderSize;
ELCEDE_FLAT.multiBorderRightSize = ELCEDE_FLAT.multiBorderSize;
ELCEDE_FLAT.multiBorderTopSize = ELCEDE_FLAT.multiBorderSize;
ELCEDE_FLAT.multiBorderBottomSize = ELCEDE_FLAT.multiBorderSize;
// image text Borders
ELCEDE_FLAT.multiTextBorderLeftSize = ELCEDE_FLAT.multiTextBorderSize;
ELCEDE_FLAT.multiTextBorderRightSize = ELCEDE_FLAT.multiTextBorderSize;
ELCEDE_FLAT.multiTextBorderTopSize = ELCEDE_FLAT.multiTextBorderSize;
ELCEDE_FLAT.multiTextBorderBottomSize = ELCEDE_FLAT.multiTextBorderSize;
//margins
ELCEDE_FLAT.multiTextLeftMargin = ELCEDE_FLAT.multiTextMargin;
ELCEDE_FLAT.multiTextRightMargin = ELCEDE_FLAT.multiTextMargin;
ELCEDE_FLAT.multiTextTopMargin = ELCEDE_FLAT.multiTextMargin;
ELCEDE_FLAT.multiTextBottomMargin = ELCEDE_FLAT.multiTextMargin;
// Focus
ELCEDE_FLAT.multiFocusBorderLeft = ELCEDE_FLAT.focusBorderLeft
ELCEDE_FLAT.multiFocusBorderRight = ELCEDE_FLAT.focusBorderRight
ELCEDE_FLAT.multiFocusBorderTop = ELCEDE_FLAT.focusBorderTop
ELCEDE_FLAT.multiFocusBorderBottom = ELCEDE_FLAT.focusBorderBottom
ELCEDE_FLAT.multiFocusShiftLeft = 0
ELCEDE_FLAT.multiFocusShiftRight = 0
ELCEDE_FLAT.multiFocusShiftTop = 0
ELCEDE_FLAT.multiFocusShiftBottom = 0
ELCEDE_FLAT.multiFocusOpacity = 1

//////////////////////////////////
//          Dialog              //
//////////////////////////////////
ELCEDE_FLAT.dialogMargins = 8
ELCEDE_FLAT.dialogWidth = 360
ELCEDE_FLAT.dialogHeight = 200
ELCEDE_FLAT.faderColor = ELCEDE_FLAT.backgroundColor
ELCEDE_FLAT.faderOpacity = 0.1
ELCEDE_FLAT.animationDuration = 500
ELCEDE_FLAT.dialogHeaderHeight = 30
// Font
ELCEDE_FLAT.dialogFontSize = ELCEDE_FLAT.buttonFontSize
ELCEDE_FLAT.dialogTitleFontSize = ELCEDE_FLAT.dialogFontSize
ELCEDE_FLAT.dialogButtonFontSize = ELCEDE_FLAT.dialogFontSize
// Borders
ELCEDE_FLAT.dialogHeaderLeftBorder = 8
ELCEDE_FLAT.dialogHeaderRightBorder = 8
ELCEDE_FLAT.dialogHeaderTopBorder = 12
ELCEDE_FLAT.dialogHeaderBottomBorder = 4
ELCEDE_FLAT.dialogMainLeftBorder = 8
ELCEDE_FLAT.dialogMainRightBorder = 8
ELCEDE_FLAT.dialogMainTopBorder = 8
ELCEDE_FLAT.dialogMainBottomBorder = 8

//////////////////////////////////
//          Meter360            //
//////////////////////////////////
ELCEDE_FLAT.meterScaleBigImage = "meter/meter360_scale_big.svg"
ELCEDE_FLAT.meterScaleSmallImage = "meter/meter360_scale_small.svg"
ELCEDE_FLAT.meterBackgroundImage = "meter/meter360_circle.svg"
ELCEDE_FLAT.meterHandImage = "meter/meter360_hand.svg"
ELCEDE_FLAT.meterSmallScaleRadiusFactor = 0.27
ELCEDE_FLAT.meterBigScaleRadiusFactor = 0.27
ELCEDE_FLAT.meterHandRadiusFactor = 0.24
ELCEDE_FLAT.meterZoneRadiusFactor = 0.31
ELCEDE_FLAT.meterOverlayImage = ""
ELCEDE_FLAT.meterFontColorMiddleText = ELCEDE_FLAT.lightColor
ELCEDE_FLAT.meterFontColorLabelText = ELCEDE_FLAT.lightColor
ELCEDE_FLAT.meterFontPointSize = ELCEDE_FLAT.fontSize
ELCEDE_FLAT.meterFontBold = false
ELCEDE_FLAT.meterValueTextPointSize = ELCEDE_FLAT.fontSize
ELCEDE_FLAT.meterFontFamily = ELCEDE_FLAT.fontFamily
ELCEDE_FLAT.meterTextVerticalOffsetFactor = 0.05
ELCEDE_FLAT.meterScaleLabelShift = 10
ELCEDE_FLAT.meterBigScaleWidthFactor = 0.01
ELCEDE_FLAT.meterBigScaleHeightFactor = 0.11
ELCEDE_FLAT.meterSmallScaleWidthFactor = 0.005
ELCEDE_FLAT.meterSmallScaleHeightFactor = 0.07
ELCEDE_FLAT.meterHandWidthFactor = 0.04
ELCEDE_FLAT.meterHandHeightFactor = 0.16
ELCEDE_FLAT.meterZonesOpacity = 0.8
ELCEDE_FLAT.meterGreenZoneColor = "#289448"
ELCEDE_FLAT.meterGreenZoneWidthFactor = 0.02
ELCEDE_FLAT.meterGreenZoneShift = 0
ELCEDE_FLAT.meterYellowZoneColor = "#F6BF00"
ELCEDE_FLAT.meterYellowZoneWidthFactor = 0.02
ELCEDE_FLAT.meterYellowZoneShift = 0
ELCEDE_FLAT.meterRedZoneColor = "#C10E1A"
ELCEDE_FLAT.meterRedZoneWidthFactor = 0.02
ELCEDE_FLAT.meterRedZoneShift = 0

//////////////////////////////////
//          LinearScale         //
//////////////////////////////////
ELCEDE_FLAT.linearScaleBigImage = "meter/linear_scale_big.svg"
ELCEDE_FLAT.linearScaleSmallImage = "meter/linear_scale_small.svg"
ELCEDE_FLAT.linearBackgroundImage = "meter/linear_scale_big.svg"
ELCEDE_FLAT.linearYStartFactor = 0.9
ELCEDE_FLAT.linearYStopFactor = 0.1
ELCEDE_FLAT.linearXStartFactor = 0.07
ELCEDE_FLAT.linearBigScaleXStart = ELCEDE_FLAT.linearXStartFactor
ELCEDE_FLAT.linearSmallScaleXStart = ELCEDE_FLAT.linearXStartFactor
ELCEDE_FLAT.linearHandRadiusFactor = 0.251
ELCEDE_FLAT.linearVerticalLineWidthFactor = 0.01
ELCEDE_FLAT.linearOverlayImage = ""
ELCEDE_FLAT.linearHandImage = "meter/linear_hand.svg"
ELCEDE_FLAT.linearFontColorMiddleText = ELCEDE_FLAT.lightColor
ELCEDE_FLAT.linearFontColorLabelText = ELCEDE_FLAT.lightColor
ELCEDE_FLAT.linearFontPointSize = ELCEDE_FLAT.fontSize
ELCEDE_FLAT.linearFontBold = false
ELCEDE_FLAT.linearValueTextPointSize = ELCEDE_FLAT.fontSize
ELCEDE_FLAT.linearFontFamily = ELCEDE_FLAT.fontFamily
ELCEDE_FLAT.linearTextVerticalOffsetFactor = 0.65
ELCEDE_FLAT.linearHandVerticalOffsetFactor = 0.6
ELCEDE_FLAT.linearScaleLabelShift = 10
ELCEDE_FLAT.linearBigScaleWidthFactor = 0.11
ELCEDE_FLAT.linearBigScaleHeightFactor = 0.009
ELCEDE_FLAT.linearSmallScaleWidthFactor = 0.07
ELCEDE_FLAT.linearSmallScaleHeightFactor = 0.005
ELCEDE_FLAT.linearHandWidthFactor = 0.06
ELCEDE_FLAT.linearHandHeightFactor = 0.048



//////////////////////////////////
//          Thermometer         //
//////////////////////////////////
ELCEDE_FLAT.thermometerFontSize = ELCEDE_FLAT.fontSize
ELCEDE_FLAT.thermometerFontFamily = ELCEDE_FLAT.fontFamily
ELCEDE_FLAT.thermometerTextVerticalOffset = 2
ELCEDE_FLAT.thermometerTextHorizontalOffset = 32
ELCEDE_FLAT.labelStepSize = 2
ELCEDE_FLAT.markerShift = 140
ELCEDE_FLAT.thermometerBigScaleWidth = 28
ELCEDE_FLAT.thermometerBigScaleHeight = 3
ELCEDE_FLAT.thermometerSmallScaleWidth = 12
ELCEDE_FLAT.thermometerSmallScaleHeight = 1
ELCEDE_FLAT.thermometerBigScaleShift = 75
ELCEDE_FLAT.thermometerMarkerWidth =  48
ELCEDE_FLAT.thermometerMarkerHeight = 35
ELCEDE_FLAT.thermometerMarkerShift = 140
ELCEDE_FLAT.thermometerSmallScaleShift = ELCEDE_FLAT.thermometerBigScaleShift + ELCEDE_FLAT.thermometerBigScaleWidth -ELCEDE_FLAT.thermometerSmallScaleWidth
ELCEDE_FLAT.thermometerLabelCenterX = 76
ELCEDE_FLAT.thermometerLabelCenterY = 296

/////////////////////////////////
//          ErrorWidget                //
/////////////////////////////////
ELCEDE_FLAT.errorWidgetFontSize = 2.0 * ELCEDE_FLAT.fontSize
ELCEDE_FLAT.errorWidgetTextFontSize = 1.2 * ELCEDE_FLAT.fontSize
ELCEDE_FLAT.errorWidgetFontFamily = ELCEDE_FLAT.fontFamily
ELCEDE_FLAT.errorWidgetFontColor = ELCEDE_FLAT.lightFontColor
ELCEDE_FLAT.errorwidgetBackgroundImage = "background_dunkel_grau.png"
ELCEDE_FLAT.errorwidgetBackgroundBorderSize = 7


ELCEDE_FLAT.errorCounterImage = "bg_error_links.png"
ELCEDE_FLAT.errorNumberImage = "bg_error_rechts.png"
ELCEDE_FLAT.errorTextImage =  ""
ELCEDE_FLAT.errorAcknowledgeImage = "background_btn_check_normal.png"
ELCEDE_FLAT.errorAcknowledgeImagePressed = "background_btn_check_aktive.png"

ELCEDE_FLAT.warningCounterImage = "bg_status_links.png"
ELCEDE_FLAT.warningNumberImage = "bg_status_rechts.png"
ELCEDE_FLAT.warningTextImage = ""
ELCEDE_FLAT.warningAcknowledgeImage = "background_btn_check_normal.png"
ELCEDE_FLAT.warningAcknowledgeImagePressed = "background_btn_check_aktive.png"

ELCEDE_FLAT.messageCounterImage = "" //"bg_status_links.png"
ELCEDE_FLAT.messageNumberImage = "" //"bg_status_rechts.png"
ELCEDE_FLAT.messageTextImage = ""
ELCEDE_FLAT.messageAcknowledgeImage = "background_btn_check_normal.png"
ELCEDE_FLAT.messageAcknowledgeImagePressed = "background_btn_check_aktive.png"

ELCEDE_FLAT.messageNextImage = "icons/next.svg"
ELCEDE_FLAT.messagePreviousImage = "icons/previous.svg"
ELCEDE_FLAT.errorwidgetErrorImage = ELCEDE_FLAT.errorImage
ELCEDE_FLAT.errorwidgetWarningImage = ELCEDE_FLAT.warningImage
ELCEDE_FLAT.errorwidgetMessageImage = ELCEDE_FLAT.messageImage

//////////////////////////////////
//           Indicator          //
//////////////////////////////////
ELCEDE_FLAT.indicatorColorOn = "double_punkt_gruen.png"
ELCEDE_FLAT.indicatorColorOff = "double_punkt_rot.png"
ELCEDE_FLAT.indicatorColor0 = "rect-red-32.png"
ELCEDE_FLAT.indicatorColor1 = "rect-green-32.png"
ELCEDE_FLAT.indicatorColor2 = "rect-gray-32.png"
ELCEDE_FLAT.indicatorColor3 = "rect-blue-32.png"

//////////////////////////////////
//        NUMERIC INPUT         //
//////////////////////////////////
// Additional Images for non symmetrical Styles
ELCEDE_FLAT.numericinputBackgroundImageNormal = "input.png"//"input_normal.png" //"background_dunkel_grau.png"
ELCEDE_FLAT.numericinputTextfieldImageEnabled = ELCEDE_FLAT.textfieldImageEnabled
ELCEDE_FLAT.numericinputTextfieldImageFocus = ELCEDE_FLAT.textfieldImageFocus
ELCEDE_FLAT.numericinputTextfieldImageDisabled = ELCEDE_FLAT.textfieldImageDisabled
ELCEDE_FLAT.numericinputTextfieldImageReadOnly = ELCEDE_FLAT.textfieldImageReadOnly
ELCEDE_FLAT.numericinputButtonLeftImageNormal = "CMinusUp.png"//"background_btn_normal_normal.png" //"background_dunkel_blau_closed_right.png"
ELCEDE_FLAT.numericinputButtonLeftImagePressed = "CMinusDown.png"//"background_btn_normal_aktive.png" //"background_dunkel_blau_closed_right.png"
ELCEDE_FLAT.numericinputButtonLeftImageDisabled = "CMinusUp.png"//"background_dunkel_blau_closed_right.png"
ELCEDE_FLAT.numericinputButtonRightImageNormal = "CPlusUp.png"//"background_btn_normal_normal.png"//"background_dunkel_blau_closed_left.png"
ELCEDE_FLAT.numericinputButtonRightImagePressed = "CPlusDown.png"//"background_btn_normal_aktive.png"//"background_dunkel_blau_closed_left.png"
ELCEDE_FLAT.numericinputButtonRightImageDisabled = "CPlusUp.png"//"background_dunkel_blau_closed_left.png"
ELCEDE_FLAT.numericinputButtonPlus = "Empty.png"//"plus.png"
ELCEDE_FLAT.numericinputButtonMinus = "Empty.png"//"minus.png"
ELCEDE_FLAT.numericinputButtonPlusMinusHeight = 16
ELCEDE_FLAT.numericinputButtonPlusMinusWidth = 16
ELCEDE_FLAT.numericinputButtonPlusMinusHorizontalAlignment = Qt.AlignHCenter
ELCEDE_FLAT.numericinputButtonPlusMinusVerticalAlignment = Qt.AlignVCenter

// font
ELCEDE_FLAT.numericinputFontSizeMiddle = ELCEDE_FLAT.fontSize;
ELCEDE_FLAT.numericinputFontColorMiddle = ELCEDE_FLAT.mediumFontColor
ELCEDE_FLAT.numericinputFontSizeLimits = ELCEDE_FLAT.fontSize / 2
ELCEDE_FLAT.numericinputFontColorLimits = ELCEDE_FLAT.lightColor

//////////////////////////////////
//        Browser               //
//////////////////////////////////
ELCEDE_FLAT.browserNextImage = "icons/next.svg"
ELCEDE_FLAT.browserBackImage = "icons/previous.svg"
ELCEDE_FLAT.browserReloadImage = "icons/view-refresh.png"
ELCEDE_FLAT.browserUrlInput = "background_textfield_normal.png"
ELCEDE_FLAT.browserUrlInputSelected = "background_textfield_selected.png"
ELCEDE_FLAT.browserHeaderBackground = "background_grey_top.png"
ELCEDE_FLAT.browserZoomInImage = "icons/ZoomIn.svg"
ELCEDE_FLAT.browserZoomOutImage = "icons/ZoomOut.svg"
ELCEDE_FLAT.browserBorderSize = 7;
// Image Borders
ELCEDE_FLAT.browserBorderLeftSize = ELCEDE_FLAT.browserBorderSize;
ELCEDE_FLAT.browserBorderRightSize = ELCEDE_FLAT.browserBorderSize;
ELCEDE_FLAT.browserBorderTopSize = ELCEDE_FLAT.browserBorderSize;
ELCEDE_FLAT.browserBorderBottomSize = ELCEDE_FLAT.browserBorderSize;

////////////////////////////////
//      FileChooser           //
////////////////////////////////
ELCEDE_FLAT.fileChooserSubcomponentBorderWidth = 4
ELCEDE_FLAT.fileChoosermainContainerColor = ELCEDE_FLAT.backgroundColor
ELCEDE_FLAT.fileChooserfontColor = ELCEDE_FLAT.mediumFontColor
ELCEDE_FLAT.fileChooserfontSelectedColor = ELCEDE_FLAT.lightFontColor
ELCEDE_FLAT.fileChooserhighlightColor = ELCEDE_FLAT.lightColor
ELCEDE_FLAT.fileChooserBackImage  = "icons/moveUp.svg"
ELCEDE_FLAT.fileChooserFolderImage  = "icons/folder.svg"
ELCEDE_FLAT.fileChooserFileImage  = "icons/file.svg"
ELCEDE_FLAT.fileChooserErrorImage = ELCEDE_FLAT.errorImage

////////////////////////////////
//      Progress Bar          //
////////////////////////////////
ELCEDE_FLAT.progressBarBackground = "progressbar_background.svg"
ELCEDE_FLAT.progressBarFilling = "progressbar_filling.svg"
ELCEDE_FLAT.progressBarBackgroundBorder = 5
ELCEDE_FLAT.progressBarBackgroundBorderLeft = ELCEDE_FLAT.progressBarBackgroundBorder
ELCEDE_FLAT.progressBarBackgroundBorderRight = ELCEDE_FLAT.progressBarBackgroundBorder
ELCEDE_FLAT.progressBarBackgroundBorderTop = ELCEDE_FLAT.progressBarBackgroundBorder
ELCEDE_FLAT.progressBarBackgroundBorderBottom = ELCEDE_FLAT.progressBarBackgroundBorder
ELCEDE_FLAT.progressBarFillingX = 2
ELCEDE_FLAT.progressBarFillingY = 2
ELCEDE_FLAT.progressBarFillingSourceWidth = 32
ELCEDE_FLAT.progressBarFillingSourceHeight = 32
ELCEDE_FLAT.progressBarTextBold = true
ELCEDE_FLAT.progressBarTextColorChoice = 0
ELCEDE_FLAT.progressBarTextOutline= true
ELCEDE_FLAT.progressBarTextOutlineColor = ELCEDE_FLAT.mediumFontColor
ELCEDE_FLAT.progressBarFillingInBackground = false

//////////////////////////////////
//             	 LOGVIEW        //
//////////////////////////////////
ELCEDE_FLAT.logFontColor0 = ELCEDE_FLAT.lightFontColor
ELCEDE_FLAT.logFontColor1 = ELCEDE_FLAT.mediumFontColor
ELCEDE_FLAT.logFontColor2 = ELCEDE_FLAT.lightColor
ELCEDE_FLAT.logFontColor3 = ELCEDE_FLAT.mediumColor
ELCEDE_FLAT.logFontSize = ELCEDE_FLAT.fontSize
ELCEDE_FLAT.logFontFamily = ELCEDE_FLAT.fontFamily

//////////////////////////////////
//       BUTTON ICONS         //
//////////////////////////////////

ELCEDE_FLAT.elcedeButton = new Object()
ELCEDE_FLAT.elcedeButton["02Position"] = "02Position.png"
ELCEDE_FLAT.elcedeButton["05ClearTable"] = "05ClearTable.png"
ELCEDE_FLAT.elcedeButton["CMinusDown"] = "CMinusDown.png"
ELCEDE_FLAT.elcedeButton["CMinusUp"] = "CMinusUp.png"
ELCEDE_FLAT.elcedeButton["CPlusDown"] = "CPlusDown.png"
ELCEDE_FLAT.elcedeButton["CPlusUp"] = "CPlusUp.png"


ELCEDE_FLAT.elcedeButton["ToolButton"] = "ToolButton.png"
ELCEDE_FLAT.elcedeButton["ToolButtonADown"] = "ToolButtonADown.png"
ELCEDE_FLAT.elcedeButton["ToolButtonAUp"] = "ToolButtonAUp.png"
ELCEDE_FLAT.elcedeButton["ToolButtonBDown"] = "ToolButtonBDown.png"
ELCEDE_FLAT.elcedeButton["ToolButtonBUp"] = "ToolButtonBUp.png"
ELCEDE_FLAT.elcedeButton["ToolButtonCDown"] = "ToolButtonCDown.png"
ELCEDE_FLAT.elcedeButton["ToolButtonCUp"] = "ToolButtonCUp.png"
ELCEDE_FLAT.elcedeButton["ToolButtonDDown"] = "ToolButtonDDown.png"
ELCEDE_FLAT.elcedeButton["ToolButtonDUp"] = "ToolButtonDUp.png"
ELCEDE_FLAT.elcedeButton["ToolButtonEDown"] = "ToolButtonEDown.png"
ELCEDE_FLAT.elcedeButton["ToolButtonEUp"] = "ToolButtonEUp.png"
ELCEDE_FLAT.elcedeButton["ToolButtonPocketType1Down"] = "ToolButtonPocketType1Down.png"
ELCEDE_FLAT.elcedeButton["ToolButtonPocketType1Up"] = "ToolButtonPocketType1Up.png"
ELCEDE_FLAT.elcedeButton["ToolButtonPocketType2Down"] = "ToolButtonPocketType2Down.png"
ELCEDE_FLAT.elcedeButton["ToolButtonPocketType2Up"] = "ToolButtonPocketType2Up.png"
ELCEDE_FLAT.elcedeButton["ToolButtonPocketType3Down"] = "ToolButtonPocketType3Down.png"
ELCEDE_FLAT.elcedeButton["ToolButtonPocketType3Up"] = "ToolButtonPocketType3Up.png"
ELCEDE_FLAT.elcedeButton["ToolButtonPocketTypeDown"] = "ToolButtonPocketTypeDown.png"
ELCEDE_FLAT.elcedeButton["ToolButtonPocketTypeUp"] = "ToolButtonPocketTypeUp.png"


ELCEDE_FLAT.elcedeButton["ErrorReset"] = "ErrorReset.png"
ELCEDE_FLAT.elcedeButton["ErrorCount"] = "ErrorCounr.png"
ELCEDE_FLAT.elcedeButton["ErrorDisplayBg"] = "ErrorDisplayBg.png"
ELCEDE_FLAT.elcedeButton["EmptyUp"] = "EmptyUp.png"
ELCEDE_FLAT.elcedeButton["EmptyDown"] = "EmptyDown.png"
ELCEDE_FLAT.elcedeButton["Date"] = "Date.png"

ELCEDE_FLAT.elcedeButtonHeight = 43
ELCEDE_FLAT.elcedeButtonWidth = 43
ELCEDE_FLAT.elcedeButtonHorizontalAlignement = Qt.AlignRight
ELCEDE_FLAT.elcedeButtonVerticalAlignement = Qt.AlignVCenter
ELCEDE_FLAT.elcedeButtonXShift = - 16
ELCEDE_FLAT.elcedeButtonYShift = 0
