// $Revision: 11492 $
.pragma library

var DialogStatus = new Object();

DialogStatus.Closed = 0;
DialogStatus.Closing = 1;
DialogStatus.Open = 2;
DialogStatus.Opening = 3;
